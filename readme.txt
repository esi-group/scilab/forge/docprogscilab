Documentation : Programming in Scilab

Abstract
--------

In this document, we present programming in Scilab.
As programming is an association between data structures and 
algorithms, the first part is dedicated to data structures, while 
the second part focuses on functions.

In the first part, we present the management of the memory 
of Scilab. 
We emphasize the features which allow to design portable script.

In the second part, we present various data types, including integers 
and strings. 
An analysis of lists and typed lists is presented.
We present a method which allows to emulate object-oriented 
programming, based on a particular use of typed lists.
We finally present how to overload basic operations on 
typed lists.

In the third part, we present features to design flexible 
functions.
We present the use of callbacks, which allow to let the user 
of a function customize a part of an algorithm. 
Then we present functions which allow to protect against wrong 
uses of a function.
We analyze methods to design functions with a variable number of 
input or output arguments, and show common ways to provide 
default values.
We show a simple method to overcome the problem caused by positional 
input arguments. 
We present the parameters module, which allows to solve the problem of 
designing a function with a large number of parameters.

In the last section, we present methods which allows to 
achieve good performances.
We emphasize the use of vectorized functions, which allow to get 
the most of Scilab performances, via calls to highly optimized numerical libraries. 
We present an in-depth analysis of the interpretor of Scilab, 
along with the BLAS, LAPACK, ATLAS and Intel MKL optimized 
numerical libraries.
We present the profiling features of Scilab and give an example of the 
vectorization of a Gaussian elimination algorithm.

TODO
----

 * Fix the section "A practical optimization example".
   Using execstr is no the only solution: add the solution based on 
   an intermediate function, with extra-arguments.
   Take the script from uncprb.
 * Use getos()=="Windows" instead of MSDOS.
 

Author
------
Copyright (C) 2008-2010 - Michael Baudin

Licence
-------

This document is released under the terms of the Creative Commons Attribution-ShareAlike 
3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/


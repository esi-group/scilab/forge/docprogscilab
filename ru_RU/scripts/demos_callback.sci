// Copyright (C) 2010 - Michael Baudin


//**************************************************
//
// Callbacks

// Using the derivative function
function y = myf ( x ) 
  y = x^2
endfunction
y = derivative ( myf , 2 )

//////////////////////////////////////////////////////

// Using a basic implementation of the derivative function 
// to explain callbacks
function y = myderivative ( f , x , h )
  y = (f(x+h) - f(x))/h
endfunction
y = myderivative ( myf , 2 , sqrt(%eps) )

//////////////////////////////////////////////////////
// A case of error

function y = myf2 ( x , a ) 
  y = a * x^2
endfunction
y = myderivative ( myf2 , 2 , sqrt(%eps) )

//**************************************************
// Avoiding the funcprot message when we transfer 
// a function into a variable.
function y = f1 ( x ) 
  y = x^2
endfunction
function y = f2 ( x ) 
  y = x^4
endfunction
f = f1
f = f2

oldfuncprot = funcprot()
funcprot(0)
f = f2
funcprot(oldfuncprot)



C http://www.nsc.liu.se/~boein/f77to90/a5.html
      Program testSizeof
      Integer testIntegerDefaultKind
      Integer(KIND=1) testIntegerKind1
      Integer(KIND=2) testIntegerKind2
      Integer(KIND=4) testIntegerKind4
      Integer(KIND=8) testIntegerKind8

      Real testRealDefaultKind
      Real(KIND=4) testRealKind4
      Real(KIND=8) testRealKind8
      Real(KIND=16) testRealKind16

      Complex testComplexDefaultKind
      Complex(KIND=4) testComplexKind4
      Complex(KIND=8) testComplexKind8
      Complex(KIND=16) testComplexKind16

      print *, "Integer"
      print *, "testIntegerKind1:", kind(testIntegerKind1)
      print *, "    BIT_SIZE:", BIT_SIZE(testIntegerKind1)      
      print *, "testIntegerKind2:", kind(testIntegerKind2)
      print *, "    BIT_SIZE:", BIT_SIZE(testIntegerKind2)      
      print *, "testIntegerKind4:", kind(testIntegerKind4)
      print *, "    BIT_SIZE:", BIT_SIZE(testIntegerKind4)      
      print *, "testIntegerKind8:", kind(testIntegerKind8)
      print *, "    BIT_SIZE:", BIT_SIZE(testIntegerKind8)      

      print *, "Real"
      print *, "testRealKind4:", kind(testRealKind4)
      print *, "    DIGITS:", DIGITS(testRealKind4)
      print *, "    EPSILON:", EPSILON(testRealKind4)
      print *, "    HUGE:", HUGE(testRealKind4)
      print *, "    MAXEXPONENT:", MAXEXPONENT(testRealKind4)
      print *, "    MINEXPONENT:", MINEXPONENT(testRealKind4)
      print *, "    PRECISION:", PRECISION(testRealKind4)
      print *, "    RADIX:", RADIX(testRealKind4)
      print *, "    RANGE:", RANGE(testRealKind4)
      print *, "    TINY:", TINY(testRealKind4)
      print *, ""
      
      print *, "testRealKind8:", kind(testRealKind8)
      print *, "    DIGITS:", DIGITS(testRealKind8)
      print *, "    EPSILON:", EPSILON(testRealKind8)
      print *, "    HUGE:", HUGE(testRealKind8)
      print *, "    MAXEXPONENT:", MAXEXPONENT(testRealKind8)
      print *, "    MINEXPONENT:", MINEXPONENT(testRealKind8)
      print *, "    PRECISION:", PRECISION(testRealKind8)
      print *, "    RADIX:", RADIX(testRealKind8)
      print *, "    RANGE:", RANGE(testRealKind8)
      print *, "    TINY:", TINY(testRealKind8)
      print *, ""
      
      print *, "testRealKind16:", kind(testRealKind16)
      print *, "    DIGITS:", DIGITS(testRealKind16)
      print *, "    EPSILON:", EPSILON(testRealKind16)
      print *, "    HUGE:", HUGE(testRealKind16)
      print *, "    MAXEXPONENT:", MAXEXPONENT(testRealKind16)
      print *, "    MINEXPONENT:", MINEXPONENT(testRealKind16)
      print *, "    PRECISION:", PRECISION(testRealKind16)
      print *, "    RADIX:", RADIX(testRealKind16)
      print *, "    RANGE:", RANGE(testRealKind16)
      print *, "    TINY:", TINY(testRealKind16)
      print *, ""

      print *, "Complex"
      print *, "testComplexKind4:", kind(testComplexKind4)
      print *, "testComplexKind8:", kind(testComplexKind8)
      print *, "testComplexKind16:", kind(testComplexKind16)
      endProgram

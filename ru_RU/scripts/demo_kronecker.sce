// Copyright (C) 2010 - Michael Baudin

// An example where smart vectorization can hurt.
// The no-loop version are slower, because of the 
// increased number of floating point operations.

function F = ti84matrixVeryslow ( m )
  //
  // Returns a m-by-m matrix of floating point integers,
  // lower triangular, with:
  // F(1,1)=1
  // F(2,1:2)=[2 3]
  // F(3,1:3)=[4 5 6]
  // F(4,1:4)=[7 8 9 10], etc...

  // Uses two nested loops.

  F = zeros(m,m)
  k = 1
  for i = 1 : m
      for j = 1 : i
          F(i,j) = k
          k = k+1
      end
  end
endfunction

function F = ti84matrix ( m )
  //
  // Uses one loop.

  F = zeros(m,m)
  ks = 1
  ke = 1
  for i = 1 : m
      F(i,1:i)=(ks:ke)
      ks = ke+1
      ke = ks+i
  end
endfunction

function F = ti84matrixKron ( m )
  //
  // Uses no loop: based on Kronecker product.

  // The first column
  r = (1:m)'
  fc = (r-1).*r./2 + 1
  // A matrix made of replications of the first column
  A = fc.*. ones(1,m)
  // A matrix or replications of the column indices
  B = ((1:m)-1).*. ones(m,1)
  // F is the lower triangular sum
  F = tril(A+B)
endfunction

function F = ti84matrixVect ( m )
  // No loop: uses only multiplication

  // The first column
  r = (1:m)'
  fc = (r-1).*r./2 + 1
  // A matrix made of replications of the first column
  A = fc* ones(1,m)
  // A matrix or replications of the column indices
  B = ones(m,1)*((1:m)-1)
  // F is the lower triangular sum
  F = tril(A+B)
endfunction

function [r,c] = ti84search ( n )
  //
  // Returns the row r and column c such that F(r,c)=n.

  // By recurrence, we find F(r,r) = (r+1)*r/2.
  // Computes r such that F(r,r)>=n.
  // We must find the smallest r such that (r+1)*r/2 >= n.
  // This is the largest positive root of:
  p = poly([-2*n 1 1],"x","coeff")
  s = roots(p)
  r = ceil(max(real(s)))
  // Compute the diagonal entry d=F(r-1,r-1).
  d = (r-1)*r/2
  // Deduce the column number.
  c = n - d
endfunction

// Compute the 10-by-10 matrix
F = ti84matrixVeryslow ( 10 )
F = ti84matrix ( 10 )
F = ti84matrixKron ( 10 )
F = ti84matrixVect ( 10 )

n = 500;
benchfun ( "Veryslow" , ti84matrixVeryslow , list( n ) , 1 , 10 );
benchfun ( "Slow" , ti84matrix , list( n ) , 1 , 10 );


n = 1000;
benchfun ( "Slow" , ti84matrix , list( n ) , 1 , 10 );
benchfun ( "Kron" , ti84matrixKron , list( n ) , 1 , 10 );
benchfun ( "Vect" , ti84matrixVect , list( n ) , 1 , 10 );


// Search (r,c) such that F(r,c)=50
n = 50
[r,c] = ti84search ( n )
// Another way to compute this: F(10,5)=50
F = ti84matrix (r);
[re,ce] = find(F==n)
if ( re==r & ce==c ) then
  disp("WIN!")
else
  disp("Boooo...")
end

// Search (r,c) such that F(r,c)=100
n = 100
[r,c] = ti84search ( n )
// Check this: F(14,9)=100
F = ti84matrix (r);
[re,ce] = find(F==n)
if ( re==r & ce==c ) then
  disp("WIN!")
else
  disp("Boooo...")
end

n = 300
[r,c] = ti84search ( n )
// Check this: F(24,24)=300
F = ti84matrix (r);
[re,ce] = find(F==n)
if ( re==r & ce==c ) then
  disp("WIN!")
else
  disp("Boooo...")
end


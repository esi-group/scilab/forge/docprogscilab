// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [noerr,msg] = check_param(params,allkeys)
  // Check that the keys of parameter list is expected.
  //
  // Calling Sequence
  //  noerr = check_param(params,allkeys)
  //  [noerr,msg] = check_param(params,allkeys)
  //
  // Parameters
  //   params: a list of parameters, with "plist" type. This list must have been initialized by a call to init_param.
  //   allkeys : a matrix of strings, the expected keys in params.
  //   noerr : a boolean, %t if all is fine, %f in case of error.
  //   msg : a string, the error message or an empty string if no error is generated. If the msg output argument is present, no error is never generated.
  //
  // Description
  //   This function allows to prevent users of a function to 
  //   make mistakes, by using a wrong key instead of an existing 
  //   key.
  //   This function is to be used in the body of the function 
  //   using the parameters module.
  //   It checks that the current list of keys in params
  //   matches the list of expected keys in allkeys.
  //   Any key in params must exist in allkeys.
  //   Not all strings in allkeys are expected to be present in params: 
  //   in this case, the default value must be used, which is not an error.
  // 
  // Example
  // function y  = f ( x , params )
  //   check_param(params,["p1" "p2"])
  //   [p1,err] = get_param(params,"p1",1)
  //   [p2,err] = get_param(params,"p2",2)
  //   y = p1 + p2 * x
  // endfunction
  // // This works.
  // params = init_param();
  // params = add_param(params,"p1",1);
  // x = 1;
  // y = f(x,params);
  // // This generates an error.
  // params = init_param();
  // params = add_param(params,"p3",1);
  // x = 1;
  // y = f(x,params);
  //
  // Author
  // 2010 - DIGITEO - Michael Baudin

  [lhs,rhs] = argn()
  if ( rhs<>2 ) then
    noerrmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d are expected."),..
      "check_param",rhs,2);
    error(noerrmsg)
  end
  if ( typeof(params) <> "plist" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: %s expected.\n"), ..
      "check_param", 1, "plist"))
  end
  if ( typeof(allkeys) <> "string" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: %s expected.\n"), ..
      "check_param", 2, "string"))
  end
  //
  // Proceed.
  // The actual keys
  currkeys = getfield(1,params)
  nkeys = size(currkeys,"*")
  noerr = %t
  msg = []
  // The key #1 is "plist".
  for i = 2 : nkeys
    k = find(allkeys==currkeys(i))
    if ( k == [] ) then
      noerr = %f
      msg = msprintf(gettext("%s: Unexpected key ""%s"" in parameter list."),"check_param",currkeys(i))
      if ( lhs < 2 ) then
        error ( msg )
      end
      break
    end
  end
endfunction


// Copyright (C) 2010 - Michael Baudin



//**************************************************
//
// Managing default values for optional arguments
// Using the empty matrix to represent a default parameter.

function y = myfun ( x , p , q )
  y = q*x^p
endfunction

myfun(3,2,1)
myfun(3)
myfun(3,2)

function y = myfun2 ( varargin )
  [lhs,rhs]=argn()
  if ( rhs<1 | rhs>3 ) then
    msg = gettext("%s: Wrong number of input arguments: %d to %d expected.\n")
    error(msprintf(msg,"myfun2",1,3))
  end
  x = varargin(1)
  pdefault = 2
  if ( rhs >= 2 ) then
    if ( varargin(2) <> [] ) then
      p = varargin(2)
    else
      p = pdefault
    end
  else
    p = pdefault
  end
  qdefault = 1
  if ( rhs >= 3 ) then
    if ( varargin(3) <> [] ) then
      q = varargin(3)
    else
      q = qdefault
    end
  else
    q = qdefault
  end
  y = q * x^p
endfunction

myfun2(2)      // same as myfun2(2,2,1) = 4
myfun2(2,3)    // same as myfun2(2,3,1) = 8
myfun2(2,3,2)  // 16
myfun2(2,[],3) // same as myfun2(2,2,3) = 12

myfun2(2) 
myfun2(2,3) 
myfun2(2,3,2)
myfun2(2,[],3) 


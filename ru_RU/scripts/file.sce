// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (c) 2008 - Michael Baudin
// Copyright (c) 2008 - Arjen Markus
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
 if computed==expected then
   flag = 1;
 else
   flag = 0;
 end
 if flag <> 1 then pause,end
endfunction

// A collection of file management utilities.
//
// References
// http://www.tcl.tk/man/tcl8.4/TclCmd/file.htm
// http://flibs.cvs.sourceforge.net/viewvc/flibs/src/filedir/m_vfile.f90?view=markup

function path = file_normalize ( name )
  // Returns a unique normalized path.
  // 
  // Calling Sequence
  // name : a 1-by-1 matrix of strings
  // path : a 1-by-1 matrix of strings
  //
  // Description
  // Returns a unique normalized path representation for the 
  // file-system object (file, directory, link, etc), whose string 
  // value can be used as a unique identifier for it.

  path = strsubst(name,"\","/")
endfunction

function i = file_firstsepindex ( name )
  //
  // Returns the index of the first separator.
  //
  // Search for platform-specific separator
  sep = filesep()
  for k = 1 : length(name)
    if ( part(name,k:k)==sep ) then
      i = k
      return
    end
  end
  //
  // Search for canonical separator
  sep = "/"
  for k = 1 : length(name)
    if ( part(name,k:k)==sep ) then
      i = k
      return
    end
  end
  i = []
endfunction

function t = file_pathtype ( name )
  // Returns the type of a path.
  // 
  // Calling Sequence
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // t : a 1-by-1 matrix of floating point integers, t=1 for absolute, t=2 for relative, t=3 for volumerelative.
  //
  // Description
  // Allows to get the type of a path.
  //
  // Examples
  // tmpfile = file_tempfile (  )
  // file_touch ( name )
  // file_pathtype ( name ) // Should be 1
  //
  // file_pathtype ( "foo.txt" ) // Should be 2
  //
  // listofvolumes = getdrives()
  // file_pathtype ( listofvolumes(1) ) // Should be 1
  
  nativename = file_nativename ( name )
  firstsep = file_firstsepindex ( name )
  platform = getos ()
  if ( firstsep==[]) then
    t = 2
  elseif (firstsep==1) then
    // There is one separator, which is the first character
    if ( platform == "Windows" ) then
      t = 3
    else
      t = 1
    end
  else
    firstcomp = part(nativename,1:firstsep-1)
    if ( firstcomp=="." | firstcomp==".." ) then
      t = 2
    else
      //
      // The first item in the path is neither a ".", nor a ".."
      // and the first character is not a separator.
      // On Linux and Mac, this is a relative file.
      // But on Windows, "toto/titi.txt" is relative and
      // "C:/titi.txt" is absolute : we have to make the difference
      // between "toto", which is just a regular directory name,
      // and "C:", which is a file volume.
      //
      listofvolumes = getdrives()
      firstcomp  = firstcomp  + "/"
      listofvolumes = strsubst(listofvolumes,"\","/")
      volumeindex = find(listofvolumes==firstcomp)
      if ( volumeindex <> [] ) then
        t = 1
      else
        t = 2
      end
    end
  end
endfunction

function path = file_nativename ( name )
  // Returns the platform-specific name of a path.
  // 
  // Calling Sequence
  // path = file_nativename ( name )
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // path : a 1-by-1 matrix of strings
  //
  // Description
  // Returns the platform-specific name of a path.
  // 
  // Examples
  // tmpfile = file_tempfile (  )
  // path = file_nativename ( tmpfile )
  // 
  
  s = filesep()
  path = strsubst(name,"\",s)
  path = strsubst(path,"/",s)
endfunction

function file_touch ( name )
    // Alter the atime and mtime of the specified files. 
    // 
    // Calling Sequence
    // file_touch ( name )
    //
    // Parameters
    // name : a 1-by-1 matrix of strings
    //
    // Description
    // Alter the atime and mtime of the specified files. 
    // TODO : fix it - does not work
    // 
    // Examples
    // name = file_tempfile (  )
    // file_touch ( name )
    // x = fileinfo ( name );
    // x(7) // The date of last change
    // file_touch ( name )
    // x = fileinfo ( name );
    // x(7) // The date of last change
    // 

    if ( fileinfo(name) <> [] ) then
        method = 2
        if ( method == 1 ) then
            tmpfile = file_tempfile (  )
            //
            // Copy the file.
            [status,message] = copyfile(name,tmpfile)
            if ( status == 0 ) then
                error(msprintf("%s: Failed to copy file %s into %s: %s","file_touch",name,tmpfile,message))
            end

            f = deletefile(name)
            if ( ~f ) then
                error(msprintf("%s: Failed to delete file %s","file_touch",name))
            end
            [status,message] = copyfile(tmpfile,name)
            if ( status == 0 ) then
                error(msprintf("%s: Failed to copy file %s into %s: %s","file_touch",tmpfile,name,message))
            end
            f = deletefile(tmpfile)
            if ( ~f ) then
                error(msprintf("%s: Failed to delete file %s","file_touch",tmpfile))
            end
        else
            fd=mopen(name,"a");
            mclose(fd);
        end
    else
        // If the file does not exist, create it as an empty file
        [fd,err]=mopen(name,"w")
        if ( err <> 0 ) then
            error(msprintf("%s: Failed to open file %s","file_touch",name))
        end
        err=mclose(fd)
        if ( err <> 0 ) then
            error(msprintf("%s: Failed to close file %s","file_touch",name))
        end
    end
endfunction


function tempfile = file_tempfile (  )
  // Returns the name of a temporary file name suitable for writing.
  // 
  // Calling Sequence
  // tempfile = file_tempfile (  )
  //
  // Parameters
  // tempfile : a 1-by-1 matrix of strings
  //
  // Description
  // The tempfile name is unique, and the file will be writable.
  // 
  // Examples
  // name = file_tempfile (  )
  // 
  
  maxtries = 10
  characterSet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", ..
	     "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", ..
             "t", "u", "v", "w", "x", "y", "z", ..
	     "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
  nrand_chars = 10
  tempfile_done = %f
  for k = 1 : maxtries
    // Computes a random file tail
    randindices = grand(1,nrand_chars,"uin",1,size(characterSet,"*"))
    randchars = characterSet(randindices)
    filetail = strcat(randchars)
    // Computes a full file and see if the file exists
    tempfile = fullfile(TMPDIR,filetail)
    if ( fileinfo(tempfile) == [] ) then
      // The file does not exist; OK
      tempfile_done = %t
      break
    end
  end
  if ( ~tempfile_done ) then
    error (msprintf("%s: Failed to generate a temporary file name","file_tempfile"))
  end
endfunction

function sep = file_canonicalsep ( )
  // Returns the platform-dependent canonical separator.
  // 
  // Calling Sequence
  // sep = file_canonicalsep ( )
  //
  // Parameters
  // sep : a 1-by-1 matrix of strings, sep="/" on Windows and Linux, sep=":" on Mac
  //
  // Description
  // Returns the platform-dependent canonical separator.
  // 
  // Examples
  // sep = file_canonicalsep ( )
  // 
  
  os = getos()
  if ( os=="Windows" ) then
    sep = "/"
  elseif ( os=="Linux" ) then
    sep = "/"
  else
    sep = ":"
  end
endfunction

function pathmat = file_split ( name  )
  // Split the path into a matrix of strings.
  // 
  // Calling Sequence
  // pathmat = file_split ( name )
  //
  // Parameters
  // name : a 1-by-1 matrix of strings
  // pathmat : a n-by-1 matrix of strings
  //
  // Description
  // Returns a matrix of string for each separator found in name.
  // 
  // Examples
  // name = file_tempfile (  )
  // pathmat = file_split ( name )
  //
  // pathmat = file_split ( "/foo/myfile.txt" )
  // 
  
  // We should use strsplit, but this does not work
  //pathmat = strsplit(name,["\" "/" ":"],256)
  sepmat(1) = filesep()
  sepmat(2) = file_canonicalsep ( )
  pathmat = []
  buff = ""
  for k = 1 : length(name)
    c = part(name,k:k)
    if ( or(c==sepmat) ) then
      pathmat($+1) = buff
      buff = ""
    else
      buff = buff + c
    end
  end
  if ( buff <> "" ) then
    pathmat($+1) = buff
  end
  pathmat(pathmat=="")=[]
endfunction

function name = file_join ( pathmat )
  // Join sub-paths from a matrix of strings.
  // 
  // Calling Sequence
  // name = file_join ( pathmat )
  //
  // Parameters
  // pathmat : a n-by-1 matrix of strings
  // name : a 1-by-1 matrix of strings
  //
  // Description
  // Returns a path made by joining the paths, using the 
  // platform-specific canonical separator, that is 
  // "/" on Windows and Linux and ":" on Mac.
  // 
  // Examples
  // name = file_join ( ["foo" "myfile.txt" ] )
  // 
  
  separator = file_canonicalsep ( )
  name = ""
  for k = 1 : size(pathmat,"*")
    name = name + separator + pathmat(k)
  end
endfunction


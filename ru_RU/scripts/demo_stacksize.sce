// Copyright (C) 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin

// exec humanreadablememory.sci;
// exec stacksizehuman.sci;

// A straightforward demo of stacksize
stacksize()
A=rand(2300,2300);
// rand: stack size exceeded (Use stacksize function to increase it).
clear A
A=rand(2200,2200);

format(25)

stacksize("max");
stacksize()

stacksizehuman ( )

A=rand(2300,2300);
stacksizehuman ( )
A=rand(5000,5000);
stacksizehuman ( )



gateway_path = get_absolute_file_path("builder.sce");

setenv (" DEBUG_SCILAB_DYNAMIC_LINK ","YES ");

libname = "fastpowgateway";
namelist = [
"fastpow" "sci_fastpow"
];
files = [
  "fastpow.c"  
  "sci_fastpow.c"  
  ];
ldflags = "";
cflags = "";
libs = [];
tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags, cflags);
clear tbx_build_gateway;


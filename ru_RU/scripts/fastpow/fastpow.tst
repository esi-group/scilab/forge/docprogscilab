// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

A = [
    5.    4.    3.    2.    1.  
    4.    4.    3.    2.    1.  
    0.    3.    3.    2.    1.  
    0.    0.    2.    2.    1.  
    0.    0.    0.    1.    1.  
];
//
B = fastpow(A,5);
E = [
    37721.    47455.    45750.    34540.    18160.  
    33940.    42751.    41245.    31150.    16380.  
    15420.    19695.    19156.    14525.    7650.   
    3720.     5010.     5020.     3861.     2045.   
    360.      570.      620.      495.      266.    
];
assert_equal(B,E);

//
B = fastpow(A,6);
E = [
    378425.    477954.    461858.    349092.    183626.  
    340704.    430499.    416108.    314552.    165466.  
    155880.    197928.    191863.    145242.    76446.   
    38640.     49980.     48972.     37267.     19656.   
    4080.      5580.      5640.      4356.      2311.    
];
assert_equal(B,E);

//
B = fastpow(A,7);
E = [
    3803941.    4811090.    4652895.    3518284.    1850955.  
    3425516.    4333136.    4191037.    3169192.    1667329.  
    1571112.    1990821.    1927497.    1458272.    767359.   
    393120.     501396.     487310.     369374.     194515.   
    42720.      55560.      54612.      41623.      21967.    
];
assert_equal(B,E);



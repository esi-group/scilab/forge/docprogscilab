
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2009 - Digiteo - Michael Baudin
//
// This file must be used under the terms of the GNU Lesser General Public License license :
// http://www.gnu.org/copyleft/lesser.html

#include "stdlib.h"
#include "stack-c.h" 
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "MALLOC.h"

//void fastpow (double * A , int n, int p, double * B);
extern int C2F(dgemm)(char *,char *,int *,int *,int *,
			    double *,double *,int *,double *,int *,
			    double *,double *,int *);
extern int C2F(dcopy)();

void fastpow (double * A , int n, int p, double * B);


// B = fastpow(A,p)
int sci_fastpow (char *fname) {
	SciErr sciErr;
	int nRowsA, nColsA;
	int nRowsp, nColsp;
	int nRowsB, nColsB;
	int iType      = 0;
	int *piAddr      = NULL;
	double * A = NULL;
	double * p = NULL;
	double * B = NULL;
	int ip;

	/* check that we have 0 parameters input */
	/* check that we have 0,1 parameters output */
	CheckRhs(2,2) ;
	CheckLhs(0,1) ;
	// Get 1st argument : A
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if(iType != sci_matrix)
	{
		Scierror(204,_("%s: Wrong type for input argument #%d: Matrix expected.\n"),fname,1);
		return 0;
	}
	sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &nRowsA, &nColsA, &A);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	CheckSquare(1,nRowsA, nColsA);
	// Get 2nd argument : p
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	sciErr = getVarType(pvApiCtx, piAddr, &iType);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	if(iType != sci_matrix)
	{
		Scierror(204,_("%s: Wrong type for input argument #%d: Matrix expected.\n"),fname,2);
		return 0;
	}
	sciErr = getMatrixOfDouble(pvApiCtx, piAddr, &nRowsp, &nColsp, &p);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	CheckScalar(2,nRowsp, nColsp);
	// Create output argument y in Scilab
	nRowsB = nRowsA;
	nColsB = nColsA;
	sciErr = allocMatrixOfDouble(pvApiCtx, Rhs + 1, nRowsB, nColsB, &B);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}
	// Compute B = A^p
	ip = (int) p[0];
	fastpow (A , nRowsA, ip, B);
	// Let Scilab know that Rhs + 1 is the 1st (and only) output argument
	LhsVar(1) = Rhs+1;
	return 0;
}


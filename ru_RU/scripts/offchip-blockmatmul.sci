function C = offchip-blockmatmul(A, B, NB)
    [m, k] = size(A)
    [k, n] = size(B)
    C = zeros(m, n)
    for ii = 1:NB:m
        I = ii:ii+NB-1
        for jj = 1:NB:n
            J = jj:jj+NB-1
            for kk = 1:NB:k
                K = kk:kk+NB-1
                C(I, J) = C(I, J) + A(I,K)*B(K, J) // on-chip
            end
        end 
    end
endfunction
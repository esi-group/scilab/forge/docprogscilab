// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

#include <stdio.h>
#include <stdlib.h>


char IsX64Platform() {
  return sizeof(size_t) == 8;
}

int
main(void)
{
  printf("short int : %2d bytes \n", sizeof(short int));
  printf("int : %2d bytes \n", sizeof(int));
  printf("int * : %2d bytes \n", sizeof(int *));
  printf("long int : %2d bytes \n", sizeof(long int));
  printf("long int * : %2d bytes \n", sizeof(long int *));
  printf("signed int : %2d bytes \n", sizeof(signed int));
  printf("unsigned int : %2d bytes \n", sizeof(unsigned int));
  printf("\n");

  printf("float : %2d bytes \n", sizeof(float));
  printf("float * : %2d bytes \n", sizeof(float *));
  printf("double : %2d bytes \n", sizeof(double));
  printf("double * : %2d bytes \n", sizeof(double *));
  printf("long double : %2d bytes \n", sizeof(long double));
  printf("\n");

  printf("signed char : %2d bytes \n", sizeof(signed char));
  printf("char : %2d bytes \n", sizeof(char));
  printf("char * : %2d bytes \n", sizeof(char *));
  printf("unsigned char : %2d bytes \n", sizeof(unsigned char));
  printf("\n");

  printf("size_t : %zd bytes\n", sizeof(size_t));
  printf("IsX64Platform : %2d\n", IsX64Platform());
  printf("\n");
  
  return EXIT_SUCCESS;
}

/* 

$ gcc demo_size_t.c
$ uname -a
Linux laptop 2.6.32-21-generic #32-Ubuntu SMP Fri Apr 16 08:10:02 UTC 2010 i686 GNU/Linux
$ ./a.out 
short int :  2 bytes 
int :  4 bytes 
int * :  4 bytes 
long int :  4 bytes 
long int * :  4 bytes 
signed int :  4 bytes 
unsigned int :  4 bytes 

float :  4 bytes 
float * :  4 bytes 
double :  8 bytes 
double * :  4 bytes 
long double : 12 bytes 

signed char :  1 bytes 
char :  1 bytes 
char * :  4 bytes 
unsigned char :  1 bytes 

size_t : 4 bytes
IsX64Platform :  0

*/
// http://msdn.microsoft.com/en-us/library/3b2e7499%28VS.80%29.aspx
// "size_t, time_t, and ptrdiff_t are 64-bit values on 64-bit Windows operating systems."
//
// http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1124.pdf
// ISO/IEC 9899:TC2, Committee Draft ? May 6, 2005
// "size_t is the unsigned integer type of the result of the sizeof operator"



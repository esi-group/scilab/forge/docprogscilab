// Copyright (C) 2010 - Michael Baudin


//**************************************************
//
// Protection against wrong uses

function y = mynorm ( A , n )
  if ( n == 1 ) then
    y = sum(abs(A))
  elseif ( n == 2 ) then
    y=sum(A.^2)^(1/2);
  elseif ( n == "inf" ) then
    y = max(abs(A))
  else
    msg = msprintf("%s: Invalid value %d for n.","mynorm",n)
    error ( msg )
  end
endfunction

mynorm([1 2 3 4],1)
mynorm([1 2 3 4],2)
mynorm([1 2 3 4],"inf")
mynorm([1 2 3 4],12)

// With protection against full matrices
function y = mynorm2 ( A , n )
  msg = msprintf("%s: Obsolete routine, please use norm instead.","mynorm2")
  warning(msg)
  if ( n == 1 ) then
    y = sum(abs(A))
  elseif ( n == 2 ) then
    y=sum(A.^2)^(1/2)
  elseif ( n == "inf" ) then
    y = max(abs(A))
  else
    msg = msprintf("%s: Invalid value %d for n.","mynorm2",n)
    error ( msg )
  end
endfunction

mynorm2([1 2 3 4],2)




//
// fast_expm -- Matrix exponential via Pade approximation.
//   References:
//   "Matrix Computations", 
//   Golub and Van Loan, 
//   Algorithm 11.3-1.
function F = fast_expm ( A )
  nA = log2(norm(A,"inf"))
  j = max(0,1+floor(nA))
  A = A/(2^j)
  // Let p be the smallest non-negative integer such that e(p,p)< delta
  p = 7
  D = eye(A)
  N = eye(A)
  X = eye(A)
  c = 1
  s = 1
  for k = 1:p
    c = c * (p-k+1) / (k*(2*p-k+1))
    X = A*X
    cX = c*X
    N = N + cX
    if ( s == 1 ) then
      D = D - cX
      s = 0
    else
      D = D + cX
      s = 1
    end
  end
  // Solve DF=N for N using Gaussian Elimination
  F = D\N
  for k = 1:j
    F = F * F 
  end
endfunction

// Find a value of p for delta = %eps
// Particular case where p=q
for p = 1:10
  f = factorial ( p );
  e = 2^(3-2*p) * f * f / factorial(2*p) / factorial ( 2*p+1 );
  disp([p e])
end

// Test correctness
A = testmatrix("frk",5)
F = fast_expm ( A )
expm(A)

// Test perfs
A=rand(500,500);
 
A=A./(sum(A,'r')*ones(500,1));
 
tic,C=expm(A);,toc
tic,D=fast_expm(A);,toc
norm(C-D)/norm(C)



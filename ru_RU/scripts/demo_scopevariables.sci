// Copyright (C) 2010 - Michael Baudin

// A demo of the scope of variables


////////////////////////////////////////////////////
// Case #1
// Read, but not write

function y = f ( x )
  y = g ( x )
endfunction

function y = g ( x )
  y = a(1) + a(2)*x + a(3)*x^2
endfunction

a = [1 2 3];
x = 2;
y = f ( x )
a

a(1) + a(2)*x + a(3)*x^2

////////////////////////////////////////////////////
// Case #1
// Fixed version

function y = gfixed ( x , a )
  y = a(1) + a(2)*x + a(3)*x^2
endfunction

function y = ffixed ( x , a )
  y = gfixed ( x , a )
endfunction

a = [1 2 3];
x = 2;
y = ffixed ( x , a )

////////////////////////////////////////////////////
// Case #2
// Write before use

function y = f2 ( x )
  y = g2 ( x )
endfunction
function y = g2 ( x )
  a = [4 5 6]
  y = a(1) + a(2)*x + a(3)*x^2
endfunction

a = [1 2 3];
x = 2;
y = f2 ( x )
a

////////////////////////////////////////////////////
// Case #3
// A bug ? We do not know...

function y = f3 ( x , a )
  b = [4 5 6]
  y = g3 ( x , a )
endfunction
function y = g3 ( x , a )
  y = b(1) + b(2)*x + b(3)*x^2
endfunction

a = [1 2 3];
x = 2;
y = f3 ( x , a )
expected = a(1) + a(2)*x + a(3)*x^2

////////////////////////////////////////////////////
// Case #4
// A bad programming practice...

// From the developer
function y = myalgorithm ( x , f , h )
  y = (f(x+h) - f(x))/h
endfunction

// From the user
function y = myfunction ( x )
  y = a(1) + a(2)*x + a(3)*x^2
endfunction

a = [1 2 3];
x = 2;
h = sqrt(%eps);
y = myalgorithm ( x , myfunction , h )
expected = a(2) + 2 * a(3)*x

// ... which can fail ...

// From the developer
function y = myalgorithm2 ( x , f , a )
  y = (f(x+a) - f(x))/a
endfunction

a = [1 2 3];
x = 2;
h = sqrt(%eps);
y = myalgorithm2 ( x , myfunction , h )
expected = a(2) + 2 * a(3)*x

// ... or worse, work with wrong results

// From the developer
function y = myalgorithm3 ( x , f , h )
  a = [4 5 6]
  y = (f(x+h) - f(x))/h
endfunction

a = [1 2 3];
x = 2;
h = sqrt(%eps);
y = myalgorithm3 ( x , myfunction , h )
expected = a(2) + 2 * a(3)*x


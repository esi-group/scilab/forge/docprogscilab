// Copyright (C) 2010 - Michael Baudin

// A demo of the argn function

function varargout = myargndemo ( varargin )
  [lhs ,rhs ]=argn()
  mprintf("lhs=%d, rhs=%d, length(varargin)=%d\n",..
    lhs,rhs,length(varargin))
  for i = 1 : lhs
    varargout(i) = 1
  end
endfunction

myargndemo();
myargndemo(1);
myargndemo(1,2);
myargndemo(1,2,3);
y1 = myargndemo(1);
[y1,y2] = myargndemo(1);
[y1,y2,y3] = myargndemo(1);




function varargout = myargndemo ( varargin )
pause
  [lhs ,rhs ]=argn()
  mprintf("lhs=%d, rhs=%d, length(varargin)=%d\n",..
    lhs,rhs,length(varargin))
  for i = 1 : lhs
    varargout(i) = 1
  end
endfunction

myargndemo();
myargndemo(1);
myargndemo(1,2);
myargndemo(1,2,3);
y1 = myargndemo(1);
[y1,y2] = myargndemo(1);
[y1,y2,y3] = myargndemo(1);




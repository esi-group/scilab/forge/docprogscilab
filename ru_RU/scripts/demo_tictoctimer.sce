// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// Demonstration of tic/toc
//

tic(); lambda = spec(rand(200,200)); t = toc()
tic(); lambda = spec(rand(200,200)); t = toc()
tic(); lambda = spec(rand(200,200)); t = toc()
//
// A loop to get reliable measures
//
for i = 1 : 10
  tic(); 
  lambda = spec(rand(200,200));
  t(i) = toc();
end
[min(t) mean(t) max(t)]
//
// Demo of timer
//
for i = 1 : 10
  timer(); 
  lambda = spec(rand(200,200));
  t(i) = timer();
end
[min(t) mean(t) max(t)]
//
// Comparing tic/toc and timer
//
stacksize("max")
n = 1000;
A = rand(n,n);
B = rand(n,n);
timer();
tic();
C = A * B;
t1 = toc();
t2 = timer();
disp([t1 t2 t2/t1])
//
// With a loop
//
stacksize("max")

timemin = 0.1;
timemax = 8.0;
nfact = 1.2;

//
// Make a loop over n
n = 1;
while ( %t )
  A = rand(n,n);
  B = rand(n,n);
  timer();
  tic();
  ierr = execstr("C = A * B","errcatch");
  t1 = toc();
  t2 = timer();
  if ( ierr <> 0 ) then
    laerr = lasterror();
    disp("Error:");
    disp(laerr);
    break
  end
  if ( and([t1 t2]> timemin ) ) then
    mprintf("User=%.3f, CPU=%.3f, CPU/User=%.3f\n",t1,t2,t2/t1)
  end
  if ( and([t1 t2]> timemax ) ) then
    break
  end
  n = ceil(nfact * n);
end


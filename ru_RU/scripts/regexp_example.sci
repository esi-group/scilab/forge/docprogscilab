// Copyright (C) 2009 - Michael Baudin


// Search in the directory for .sci files and apply the given function.
function filematrix = searchSciFilesInDir ( directory , funname )
  filematrix = []
  lsmatrix = ls ( directory )';
  for f = lsmatrix
    issci = regexp(f,"/(.*).sci/");
    issce = regexp(f,"/(.*).sce/");
    if ( issci <> [] | issce <> [] ) then
      scifile = fullfile ( directory , f )
      funname ( scifile )
      filematrix($+1) = scifile
    end
  end
endfunction

// Sample uses
function mydisplay ( filename )
  [path,fname,extension]=fileparts(filename)
  mprintf ("%s%s\n",fname,extension)
endfunction

directory = fullfile(SCI,"modules","polynomials","macros")
filematrix = searchSciFiles ( directory , mydisplay );


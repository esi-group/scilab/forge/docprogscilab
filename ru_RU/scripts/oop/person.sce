// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A person class

function this = person_new ()
  this = tlist(["TPERSON","name","firstname","phone","email"])
  this.name=""
  this.firstname=""
  this.phone=""
  this.email=""
endfunction

function this = person_free (this)
  // Нет никаких действий.
endfunction

function this = person_configure (this,key,value)
  select key
  case "-name" then
    this.name = value
  case "-firstname" then
    this.firstname = value
  case "-phone" then
    this.phone = value
  case "-email" then
    this.email = value
  else
    errmsg = sprintf("Неизвестный ключ %s",key)
    error(errmsg)
  end
endfunction

function value = person_cget (this,key)
  select key
  case "-name" then
    value = this.name
  case "-firstname" then
    value = this.firstname
  case "-phone" then
    value = this.phone
  case "-email" then
    value = this.email
  else
    errmsg = sprintf("Неизвестный ключ %s",key)
    error(errmsg)
  end
endfunction

function person_display (this)
  mprintf("Person\n")
  mprintf("Name: %s\n", this.name)
  mprintf("First name: %s\n", this.firstname)
  mprintf("Phone: %s\n", this.phone)
  mprintf("E-mail: %s\n", this.email)
endfunction

p1 = person_new();
p1=person_configure(p1,"-name","Backus");
p1=person_configure(p1,"-firstname","John");
p1=person_configure(p1,"-phone","01.23.45.67.89");
p1=person_configure(p1,"-email","john.backus@company.com");
person_display(p1)
name = person_cget(p1,"-name")
p1 = person_free(p1);

////////////////////////////////////////////////////////
// Перегрузка

function str = %TPERSON_string (this)
  str = []
  k = 1
  str(k) = sprintf("Person:")
  k = k + 1
  str(k) = sprintf("======================")
  k = k + 1
  str(k) = sprintf("Name: %s", this.name)
  k = k + 1
  str(k) = sprintf("First name: %s", this.firstname)
  k = k + 1
  str(k) = sprintf("Phone: %s", this.phone)
  k = k + 1
  str(k) = sprintf("E-mail: %s", this.email)
endfunction

function %TPERSON_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

p1 = person_new();
p1=person_configure(p1,"-name","Backus");
p1=person_configure(p1,"-firstname","John");
p1=person_configure(p1,"-phone","01.23.45.67.89");
p1=person_configure(p1,"-email","john.backus@company.com");
//person_display(p1)
p1
string(p1)
disp(p1)
p1 = person_free(p1);

////////////////////////////////////////////////////////////////////
// Extending the class

function this = company_new ()
  this = tlist(["TCOMPANY","name","address","purpose","employees"])
  this.name=""
  this.address=""
  this.purpose=""
  this.employees = list()
endfunction

function this = company_free (this)
  for i = 1 : length(this.employees)
    this.employees(i) = person_free(this.employees(i))
  end
endfunction

function this = company_addperson ( this , person )
  this.employees($+1) = person
endfunction



function this = company_configure (this,key,value)
  select key
  case "-name" then
    this.name = value
  case "-address" then
    this.address = value
  case "-purpose" then
    this.purpose = value
  else
    errmsg = sprintf("Неизвестный ключ %s",key)
    error(errmsg)
  end
endfunction

function value = person_cget (this,key)
  select key
  case "-name" then
    value = this.name
  case "-address" then
    value = this.address
  case "-purpose" then
    value = this.purpose
  else
    errmsg = sprintf("Неизвестный ключ %s",key)
    error(errmsg)
  end
endfunction

function str = %TCOMPANY_string (this)
  str = []
  k = 1
  str(k) = sprintf("Company:")
  k = k + 1
  str(k) = sprintf("======================")
  k = k + 1
  str(k) = sprintf("Name: %s", this.name)
  k = k + 1
  str(k) = sprintf("Address: %s", this.address)
  k = k + 1
  str(k) = sprintf("Purpose: %s", this.purpose)
  k = k + 1
  str(k) = sprintf("Employees: %d", length(this.employees))
  for i = 1 : length(this.employees)
    k = k + 1
    str(k) = sprintf("Employee #%d", i)
    estr = string(this.employees(i))
    nrows = size(estr,"r")
    for j = 1 : nrows
      k = k + 1
      str(k) = estr(j)
    end
  end
endfunction

function %TCOMPANY_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

////////////////////////////////////////////////////////
// Использование

p1 = person_new();
p1=person_configure(p1,"-name","Backus");
p1=person_configure(p1,"-firstname","John");
p1=person_configure(p1,"-phone","01.23.45.67.89");
p1=person_configure(p1,"-email","john.backus@company.com");
p1

p2 = person_new();
p2=person_configure(p2,"-name","Newton");
p2=person_configure(p2,"-firstname","Isaac");
p2=person_configure(p2,"-phone","98.76.54.32.10");
p2=person_configure(p2,"-email","isaac.newton@company.com");
p2

c1 = company_new ();
c1=company_configure(c1,"-name","Good Stuff");
c1=company_configure(c1,"-address","Nice Beach (California)");
c1=company_configure(c1,"-purpose","Having Fun");
c1
c1 = company_addperson(c1,p1)
c1 = company_addperson(c1,p2)

p1 = person_free(p1);
p2 = person_free(p2);

c1
c1 = company_free(c1);



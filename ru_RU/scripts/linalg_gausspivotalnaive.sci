// Copyright (C) 2008 - 2010 - Michael Baudin


// linalg_gausspivotalnaive --
//   Returns the solution of Ax = b
//   with a Gauss elimination and row exchanges.
//   This algorithm might be sensitive to inaccuracies if A near singular.
//   There is no solution is A is singular.
//   If A is not ill-conditionned, the algorithm is accurate.
//   Uses a naive algorithm with all loops expanded (no vectorization).
function x = gausspivotalnaive ( A , b ) 
  n = size(A,"r")
  // Initialize permutation
  P=zeros(1,n-1)
  // Perform Gauss transforms
  for k=1:n-1
    // Search pivot
    mu = k
    abspivot = abs(A(mu,k))
    for i=k:n
      if (abs(A(i,k))>abspivot) then
        mu = i
        abspivot = abs(A(i,k))
      end
    end
    // Swap lines k and mu from columns k to n
    for j=k:n
      tmp = A(k,j)
      A(k,j) = A(mu,j)
      A(mu,j) = tmp
    end
    P(k) = mu
    // Perform transform for lines k+1 to n
    for i=k+1:n
      A(i,k) = A(i,k)/ A(k,k)
    end
    for i = k+1:n
      for j = k+1:n
        A(i,j) = A(i,j) - A(i,k) * A(k,j)
      end
    end
  end
  // Perform forward substitution
  for k=1:n-1
    // Swap b(k) and b(P(k))
    tmp = b(k)
    mu = P(k)
    b(k) = b(mu)
    b(mu) = tmp
    // Substitution
    for i=k+1:n
      b(i) = b(i) - b(k) * A(i,k)
    end
  end
  // Perform backward substitution
  for k=n:-1:1
    t = 0.
    for j=k+1:n
      t = t + A(k,j) * b(j)
    end
    b(k) = (b(k) - t) / A(k,k)
  end
  x = b
endfunction



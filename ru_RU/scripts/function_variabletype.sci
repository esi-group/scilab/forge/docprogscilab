// Copyright (C) 2010 - Michael Baudin

//
// Functions with variable input argument type

function myprint ( X )
  if ( type(X) == 1 ) then
    disp("Double matrix")
  elseif ( type(X) == 4 ) then
    disp("Boolean matrix")
  else
    error ( "Unexpected type." )
  end
endfunction

myprint ( [1 2] )
myprint ( [%T %T] )


// myprint --
// An example of a function which takes several types of 
// input arguments
//
// Parameters
//   var: if var is a matrix of doubles, prints this matrix as a ready-to-read format
//     if var is a matrix of doubles, prints this matrix with T/F statements
function myprint ( X )
  if ( type(X) == 1 ) then
    // Real matrix
    for i = 1 : size(X,"r")
      for j = 1 : size(X,"c")
        mprintf("%-5d ",X(i,j))
        if ( j == 3 ) then
          mprintf("    ")
        end
      end
      mprintf("\n")
    end
  elseif ( type(X) == 4 ) then
    // Boolean matrix
    for i = 1 : size(X,"r")
      for j = 1 : size(X,"c")
        mprintf("%s ",string(X(i,j)))
        if ( j == 3 ) then
          mprintf("  ")
        end
      end
      mprintf("\n")
    end
    
  else
    error ( "Unexpected type for input argument var." )
  end
endfunction


X = [
1 2 3 4 5 6
7 8 9 10 11 12
13 14 15 16 17 18
];

myprint ( X )

X = [
%T %T %F %F %F %F 
%T %F %F %T %T %T
%F %T %F %F %T %F
];

myprint ( X )


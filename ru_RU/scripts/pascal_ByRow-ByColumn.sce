// Copyright (C) 2009 - 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


/////////////////////////////////////////////////////////////
// Pascal up matrix.

// See the discussion at :
// http://bugzilla.scilab.org/show_bug.cgi?id=7670
// http://en.wikipedia.org/wiki/Pascal_matrix

// Column by column version
function c = pascalup_col (n)
   c = eye(n,n)
   c(1,:) = ones(1,n)
   for i = 2:(n-1)
      c(2:i,i+1) = c(1:(i-1),i)+c(2:i,i)
   end
endfunction

// Uses a row by row implementation with 1-loop.
// Calixte Denizet.
// Same implementation was suggested by Samuel Gougeon.
function c = pascallow_row ( n )
   c = eye(n,n)
   c(:,1) = ones(n,1)
   for i = 2:(n-1)
      c(i+1,2:i) = c(i,1:(i-1))+c(i,2:i)
   end
endfunction

pascalup_col (5)
pascallow_row ( 5 )

stacksize("max");
scf();
xtitle("Pacal Matrix","Matrix order (n)","Time By Column / Time By Row");
timemin = 0.1
timemax = 4.0
nfact = 1.2;
n = 1;
k = 1;
while ( %t )
  tic();
  P = pascalup_col (n);
  t1 = toc();
  tic();
  P = pascallow_row (n);
  t2 = toc();
  if ( and([t1 t2] > timemin) ) then
    plot(n,t1/t2,"rx")
  end
  if ( and([t1 t2] > timemax) ) then
    break
  end
  k = modulo(k+1,10);
  if ( k == 0 ) then
    n = ceil(nfact * n);
  end
end




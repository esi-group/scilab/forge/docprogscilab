// Copyright (C) 2010 - Michael Baudin

// Define a polynomial by its roots, by its coefficients
p=poly([1 2],"x")
q=poly([1 2],"x","coeff")
p+q
p*q
q/p
//
// Evaluating a polynomial with horner
p=poly([1 2],"x")
horner(p,[0 1 2 3])
//
// Create the characteristic polynomial associated with a 
// square matrix
A = [1 2;3 4]
p = poly(A,"x")
// 
// Check this result

//
// Get the same polynomial by another way
px = poly([0 1],"x","coeff")
p = det(A-px*eye())


Miscellaneous
-------------
datafile1.txt : a simple text data file with 2 rows and 3 columns
datafile2.txt  : a simple text data file with 2 rows and 3 columns
demos_string.sci : A demo of strings in Scilab.
demos_tlist.sci : A demo of tlists in Scilab.
demo_hypermatrices.sce : A demo of hypermatrices
demo_kronecker.sce : An example where smart vectorization can hurt.
    The no-loop version are slower, because of the 
    increased number of floating point operations.
demo_lists.sci : A demo for lists
demo_polynomials.sce : a demo of polynomials
demo_sizeof.f  : A Fortran source code to see the size of integers, and floats.
demo_sizeof.log.txt : The log file for demo_sizeof.f
demo_size_t.c : A C source code to see the size of integers, and floats.
examples-2Dplot_title.sce : A sample 2D plot with title.
exercise_execstr_dataset.sce : A demo of the execstr function when it is used to 
    hide a set of functions.
file.sce : A collection of file management utilities (normalize, filetype, touch, 
    tempfile, canonicalsep, split, join).
regexp_example.sci : A demo of the regexp function.

Functions and argument checking
-------------------------------

Some of the functions in this directory are available, in improved form, 
in the apifun module:

http://atoms.scilab.org/toolboxes/apifun

check_param.sci  : Check that the keys of parameter list is expected.
demos_advancedfunctions.sci : Some examples of advanced function topics.
demos_callback.sci : A demo of callbacks
demos_defaultargemptymatrix.sci : Managing default values for optional 
   arguments - Using the empty matrix to represent a default parameter.
demos_protectionwronguses.sci : Robust functions with protection against wrong uses
demo_argn.sci : A demo of the argn function
demo_execstr_deff.sce : A demo of the execstr function
demo_feval.sce : Examples of use of the feval function.
demo_function_addargslist.sce : A demo of calling back a function which may have 
    additionnal arguments using a list
demo_function_robust.sce : A demo of a robust function with argument 
    checking (type, size, content)
demo_function_vargn.sci : A demo of a function which takes a variable number of input 
    arguments
demo_parameters.sci : a demo of the "parameters" module
demo_scopeofvariables.sci Demonstration of the scope of variables
    through the call stack.
demo_scopevariables.sci : A demo of the scope of variables
function_variabletype.sci : Functions with variable input argument type.
keyValuePairs.sce : A function to manage (key,value) pairs.
optionalargs_exists.sce : Shows why using the key=value to provide optional 
    arguments is sometimes a poor programming practice.
demo_issuecallback.sci : Two examples of failures with callbacks and their workarounds.

Performances and benchmarking
-----------------------------

Some of the functions in this directory are available, in improved form, 
in the scibench module 
(e.g. benchfun, and various benchmarks):

http://atoms.scilab.org/toolboxes/scibench

and the specfun module (e.g. pascal matrix, combination functions):

http://atoms.scilab.org/toolboxes/specfun

benchfun.sci : Benchmarks a function and measure its performance.
benchfun_demo.sce : A demo of the benchfun function on Pascal 
    matrix and a subset function
bench_backslash.sce : Benchmarking the backslash operator.
bench_dynamicmatrix.sce : Benchmarks a function and measure its performance.
bench_matmul.sce : Benchmarking the matrix-matrix product
block_matmul.sce : A blocked matrix multiply in Scilab.
demo_profiling.sce : a demo of a profiling
demo_tictoctimer.sce : Demonstration of tic/toc
pascal_ByRow-ByColumn.sce : Compute the Pascal matrix either by row or by column.
linalg_gausspivotal.sci : Returns the solution of Ax = b with a Gaussian elimination 
    algorithm with row pivoting. A vectorized implementation.
linalg_gausspivotalnaive.sci : Solves the linear equation A*x=b with a Gaussian elimination 
    algorithm with row pivoting. A naive implementation.
demo_kroneckercombinate.sce : A simplified demo of the Kronecker product, used to vectorize 
    a function producing combinations.

Memory and stack
----------------
stacksizehuman.sci : Prints a human-readable state of the stack.
humanreadablememory.sci : Converts a memory in bytes into a human-readable string.
demo_stacksize.sce : A straightforward demo of stacksize

Numerical algorithms
--------------------

Some of the functions in this directory are available, in improved form, 
in the linalg module
(e.g. fast power, matrix exponential, naive and non-naive Gaussian elmination) :

http://atoms.scilab.org/toolboxes/linalg

exercise_power.sce : How to compute a^p ?
    What is the complexity of the algorithm ?
expmatrix.sci : Algorithms to compute the exponential of a matrix.

A design problem
----------------

This is the content of the "designissue" directory.

This exercise was used to clarify the design of the Low Discrepancy toolbox. 

designissue/sequence.sce: A script to present three different solutions #1, 2 and 3 
    to compute sequences converging toward the Golden ratio : (1+sqrt(5))/2 = 1.61803398874989484820458683436563811772...
	The problem is to be able to manage three different sequences A, B, C, 
	with a clean interface and the smallest possible code duplication. 
	Each sequence has an initial point and a iterative update. 
	The three solutions #1, 2 and 3 are all inspired by Object Oriented Programming. 
designissue/solution1.svg : An Inkscape diagram to present the design solution #1
designissue/solution2.svg : An Inkscape diagram to present the design solution #2
designissue/solution3.svg : An Inkscape diagram to present the design solution #3

Fast Power
----------

This is the content of the "fastpow" directory.

This provides a fast power algorithm, based on a 
C source code. 

This is essentially the same as the one provided in linalg:

http://atoms.scilab.org/toolboxes/linalg

fastpow/builder.sce : the builder script. Execute this to build the gateway.
fastpow/cleaner.sce : the cleaner script. Execute this to delete binary files.
fastpow/fastpow.c : the C source code.
fastpow/fastpow.tst : the unit test of the fastpow function.
fastpow/power.tst : the unit test of the ^ operator in Scilab.
fastpow/sci_fastpow.c : the gateway to fastpow.c

Object Oriented Programming
---------------------------

The "oop" directory contains the "person.sce" script which shows how to 
emulate OOP in Scilab.
 * Creating the basic tlist.
 * Overloading the type to manage the printing.
 * Extending the class with derived classes.

// Copyright (C) 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2008 - ENPC - Bruno Pincon
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Bibliography
// Bruno Pincon, 2008, "Quelques tests de rapidite entre differents logiciels matriciels"
// http://gitweb.scilab.org/?p=scilab.git;a=blob_plain;f=scilab/modules/cacsd/tests/nonreg_tests/bug_68.tst;h=920d091d089b61bf961ea9e888b4d7d469942a14;hb=4ce3d4109dd752fce5f763be71ea639e09a12630

params = []
get_param(params,"direction",%t)
// !--error 10000 
//get_param: Wrong type for input argument #1: plist expected.
//at line      40 of function get_param called by :  
//get_param(params,"direction",%t)
params = []
[direction,err] = get_param(params,"direction",%t)


function order = mycompfun ( x , y )
  if ( modulo(x,2) == 0 & modulo(y,2) == 1 ) then
    // even < odd
    order = -1
  elseif ( modulo(x,2) == 1 & modulo(y,2) == 0 ) then
    // odd > even
    order = 1
  else
    // 1<3, or 2<4
    if ( x < y ) then
      order = -1
    elseif ( x==y ) then
      order = 0
    else 
      order = 1
    end
  end
endfunction


params = init_param()
params = add_param(params,"direction",%f);
[direction,err] = get_param(params,"direction",%t)


// An implementation of a merge-sort algorithm, based on the 
// parameters module.




// Calling Sequence
//   y = mergesort ( x )
//   y = mergesort ( x , params )
function y = mergesort ( varargin )
  [lhs,rhs]=argn()
  if ( and( rhs<>[1 2] ) ) then
    errmsg = sprintf(..
      "%s: Unexpected number of arguments : "+..
      "%d provided while %d to %d are expected.",..
      "mergesort",rhs,1,2);
    error(errmsg)
  end
  x = varargin(1)
  if ( rhs<2 ) then
    params = []
  else
    params = varargin(2)
  end
  [direction,err] = get_param(params,"direction",%t)
  [compfun,err] = get_param(params,"compfun",compfun_default)
  y = mergesortre ( x , direction , compfun )
endfunction
function x = mergesortre ( x , direction , compfun )
  n = length(x)
  indices = 1 : n
  if ( n > 1 ) then
    m = floor(n/2)
    p = n-m
    x1 = mergesortre ( x(1:m) , direction , compfun )
    x2 = mergesortre ( x(m+1:n) , direction , compfun )
    x = merge ( x1 , x2 , direction , compfun )
  end
endfunction
// Merge the two lists x1 and x2 into x.
function x = merge ( x1 , x2 , direction , compfun )
    n1 = length(x1)
    n2 = length(x2)
    n = n1 + n2
    x = []  
    i = 1 
    i1 = 1
    i2 = 1
    for i = 1:n
      order = compfun ( x1(i1) , x2(i2) )
      if ( ~direction ) then
        order = -order
      end
      if ( order<=0 ) then
        x(i) = x1(i1)
        i1 = i1+1
        if (i1 > m) then
          x(i+1:n) = x2(i2:p)
          break
        end
      else
        x(i) = x2(i2)
        i2 = i2+1
        if (i2 > p) then
          x(i+1:n) = x1(i1:m)
          break
        end
      end
    end
endfunction

function order = compfun_default ( x , y )
  if ( x < y ) then
    order = -1
  elseif ( x==y ) then
    order = 0
  else 
    order = 1
  end
endfunction


// With default settings
x = [4 5 1 6 2 3]';
mergesort ( x )
// With customized direction
params = init_param();
params = add_param(params,"direction",%f);
mergesort ( x , params )
// With customized comparison function
params = init_param();
params = add_param(params,"compfun",mycompfun);
mergesort ( [4 5 1 6 2 3]' , params )

// A wrong call
params = init_param();
params = add_param(params,"compffun",mycompfun);
mergesort ( [4 5 1 6 2 3]' , params )

//////////////////////////////////////////////////////////////
// Fixed parameters module

// Calling Sequence
//   y = mergesort ( x )
//   y = mergesort ( x , params )
function y = mergesort ( varargin )
  [lhs,rhs]=argn()
  if ( and( rhs<>[1 2] ) ) then
    errmsg = sprintf(..
      "%s: Unexpected number of arguments : "+..
      "%d provided while %d to %d are expected.",..
      "mergesort",rhs,1,2);
    error(errmsg)
  end
  x = varargin(1)
  if ( rhs<2 ) then
    params = []
  else
    params = varargin(2)
  end
  check_param(params,["direction" "compfun"])
  [direction,err] = get_param(params,"direction",%t)
  [compfun,err] = get_param(params,"compfun",compfun_default)
  y = mergesortre ( x , direction , compfun )
endfunction

// A wrong call : generates an error
params = init_param();
params = add_param(params,"compffun",mycompfun);
mergesort ( [4 5 1 6 2 3]' , params )


// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// A blocked matrix multiply in Scilab.
// Reference : "Automated Empirical Optimization of Software and the ATLAS Project", Whaley, Petitet, Dongarra - 2000

// http://www.cs.indiana.edu/classes/p573/notes/arch/07_blockmatmat.html

function C = blockmatmul(A, B, NB)

    // Performes a blocked matrix multiply.
    // 
    // Parameters
    //   A : a m-by-k matrix of doubles
    //   B : a k-by-n matrix of doubles
    //   NB : a 1-by-1 matrix of doubles, the size of the blocks. We make the assumption that NB divides n, k, m.
    //   C : a m-by-n matrix of doubles
    //
    // Description
    // Cuts the arrays into subblocks of size NB x NB. 
    // In a compiled language (C or Fortran), this allows enhancing cache re-use. 
    // Uses a straightforward I-J-K version.

    [m, k1] = size(A)
    [k2, n] = size(B)
    if (k1 <> k2) then
      localstr = gettext("%s: The second dimension of A is %d while the first dimension of B is %d")
      error(msprintf(localstr,"blockmatmul",k1,k2))
    end
    k = k1
    C = zeros(m, n)

    for ii = 1:NB:m
        I = ii:ii+NB-1
        for jj = 1:NB:n
            J = jj:jj+NB-1
            for kk = 1:NB:k
                K = kk:kk+NB-1
                C(I, J) = C(I, J) + A(I,K)*B(K, J)
            end
        end 
    end
endfunction

m = 2*grand(1,1,"uin",1,10);
n = 2*grand(1,1,"uin",1,10);
k = 2*grand(1,1,"uin",1,10);
NB = 2;
A = grand(m,k,"uin",1,10),
B = grand(k,n,"uin",1,10),
C1 = A*B
C2 = blockmatmul(A,B,NB)
and(C1==C2)


function C = blockmatmul2(A, B, NB)

    // Performes a blocked matrix multiply.
    // 
    // Parameters
    //   A : a m-by-k matrix of doubles
    //   B : a k-by-n matrix of doubles
    //   NB : a 1-by-1 matrix of doubles, the size of the blocks. We make the assumption that NB divides n, k, m.
    //   C : a m-by-n matrix of doubles
    //
    // Description
    // Cuts the arrays into subblocks of size NB x NB. 
    // In a compiled language (C or Fortran), this allows enhancing cache re-use. 
    // Uses a I-K-J version with transpose.

    [m, k1] = size(A)
    [k2, n] = size(B)
    if (k1 <> k2) then
      localstr = gettext("%s: The second dimension of A is %d while the first dimension of B is %d")
      error(msprintf(localstr,"blockmatmul",k1,k2))
    end
    k = k1
    C = zeros(m, n)

    for ii = 1:NB:m
        I = ii:ii+NB-1
        for kk = 1:NB:k
            K = kk:kk+NB-1
            T = A(I,K)'
            for jj = 1:NB:n
                J = jj:jj+NB-1
                C(I, J) = C(I, J) + T'*B(K, J)
            end
        end 
    end
endfunction

m = 2*grand(1,1,"uin",1,10);
n = 2*grand(1,1,"uin",1,10);
k = 2*grand(1,1,"uin",1,10);
NB = 2;
A = grand(m,k,"uin",1,10),
B = grand(k,n,"uin",1,10),
C1 = A*B
C2 = blockmatmul2(A,B,NB)
and(C1==C2)



// Copyright (C) 2009 - Michael Baudin

// configure title and axis
function f = myquadratic ( x )
  f = x.^2
endfunction
xdata = linspace ( 1 , 10 , 50 );
ydata = myquadratic ( xdata );
plot ( xdata , ydata )
// With title
title ( "My title" );
// With xtitle
xtitle ( "My title" , "X axis" , "Y axis" );
// With the handles
h = gca()
h.x_label.text = "X axis"
h.y_label.text = "Y axis"

// Configure legend with the legend function
function f = myquadratic ( x )
  f = x.^2
endfunction
function f = myquadratic2 ( x )
  f = 2 * x.^2
endfunction
xdata = linspace ( 1 , 10 , 50 );
ydata = myquadratic ( xdata );
plot ( xdata , ydata , "+-" )
ydata2 = myquadratic2 ( xdata );
plot ( xdata , ydata2 , "o-" )
xtitle ( "My title" , "X axis" , "Y axis" );
legend ( "x^2" , "2x^2" );


// Configure legend with handles
function f = myquadratic ( x )
  f = x.^2
endfunction
function f = myquadratic2 ( x )
  f = 2 * x.^2
endfunction
xdata = linspace ( 1 , 10 , 50 );
ydata = myquadratic ( xdata );
plot ( xdata , ydata )
ydata2 = myquadratic2 ( xdata );
plot ( xdata , ydata2 )
h = gca();
h.title.text = "My title"
h.x_label.text = "X axis"
h.y_label.text = "Y axis"
legend ( "x^2" , "2x^2" );
h=gcf();
h.children.children(2).children.mark_mode="on";
h.children.children(2).children.mark_size= 10;
h.children.children(2).children.mark_style= 1;
// Show that the fields are in the order of the creation of the plot
disp(h.children.children(1).text)

// Show all possible styles of marks
xdata = linspace ( 1 , 10 , 50 );
ydata = myquadratic ( xdata );
plot ( xdata , ydata )
h=gcf();
h.children.children.children.mark_mode="on";
h.children.children.children.mark_size= 10;
for i = 0:14
  h.children.children.children.mark_style= i;
  mprintf("Current style=%d\n", i)
  pause
end


// Copyright (C) 2010 - DIGITEO - Michael Baudin

function mstr = humanreadablememory ( varargin )
  // Converts a memory in bytes into a human-readable string.
  //
  // Calling Sequence
  //   mstr = humanreadablememory ( m )
  //   mstr = humanreadablememory ( m , unitsystem )
  //
  // Parameters
  //   sz : a 1-by-1 matrix of floating point integers, the number of doubles in the stack
  //   unitsystem : a 1-by-1 matrix of booleans, unitsystem=%t for decimal, unitsystem=%f for binary (Default unitsystem=%f, i.e. decimal)
  //   mstr : a 1-by-1 matrix of strings, a human readable memory memory 
  //
  // Description
  //   Converts a memory in bytes into a human-readable string.
  //
  //   If unitsystem=%t (decimal), the number of bytes in one KB is 1000.
  //   If unitsystem=%f (binary), the number of bytes in one KB is 1024.
  //
  //   Internally, uses the format "%.1f" to format the memory into a float.
  //
  // Example
  //   humanreadablememory ( 40000000 ) // "40.0 MB"
  //   humanreadablememory ( 1224431232 ) // "1.2 GB"
  //   humanreadablememory ( 1224431232 , %f ) // "1.1 GiB"
  //   humanreadablememory ( 624431232 ) // "624.4 MB"
  //

  [lhs,rhs] = argn()
  if ( and(rhs<>[1 2]) ) then
    noerrmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
      "stacksize2memory",rhs,1,2);
    error(noerrmsg)
  end
  m = varargin(1)
  if ( rhs < 2 ) then
    unitsystem = %t
  else
    unitsystem = varargin(2)
  end
  //
  // Check type
  if ( typeof(m) <> "constant" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "stacksize2memory", 1, "m" , typeof(m) , "constant"))
  end
  if ( typeof(unitsystem) <> "boolean" ) then 
    error(sprintf(gettext("%s: Wrong type for input argument #%d: variable %s has type %s while %s is expected.\n"), ..
      "stacksize2memory", 2, "unitsystem" , typeof(unitsystem) , "boolean"))
  end
  //
  // Check size
  if ( size(m,"*") <> 1 ) then 
    error(sprintf(gettext("%s: Wrong size for input argument #%d: variable %s has size %d while %d is expected.\n"), ..
      "stacksize2memory", 1, "m" , size(m,"*") , 1))
  end
  if ( size(unitsystem,"*") <> 1 ) then 
    error(sprintf(gettext("%s: Wrong size for input argument #%d: variable %s has size %d while %d is expected.\n"), ..
      "stacksize2memory", 2, "unitsystem" , size(unitsystem,"*") , 1))
  end
  //
  // Proceed...
  if ( unitsystem ) then
    kilobytes = 1000
    unitnames = [
      "B"
      "KB"
      "MB"
      "GB"
      "TB"
      "PB"
      "EB"
      ]
  else
    kilobytes = 1024
    unitnames = [
      "B"
      "KiB"
      "MiB"
      "GiB"
      "TiB"
      "PiB"
      "EiB"
    ]
  end
  power2unit = [3 6 9 12 15 18]
  // Compute the power of the memory
  mempow = floor(log10(m))
  // Compute the index of the unit from the power
  booltable = ~(mempow<power2unit)
  booltable(find(~booltable)) = []
  index = length(booltable) + 1
  // Find the unit name from the index
  unit = unitnames(index)
  // Divide the memory by the corresponding power
  m = m / kilobytes^(index-1)
  mstr = msprintf("%.1f %s",m,unit)
endfunction


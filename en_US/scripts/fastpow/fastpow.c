
/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "stack-c.h" 
#include "MALLOC.h"

extern int C2F(dgemm)(char *,char *,int *,int *,int *,
			    double *,double *,int *,double *,int *,
			    double *,double *,int *);
extern int C2F(dcopy)();

// fastpow--
//   Compute a fast matrix power A^p
// Arguments
//   A : a square AxA matrix
//   n : the dimension of the matrix
//   p : a positive, nonzero integer
//   B : the matrix A^p
void fastpow (double * A , int n, int p, double * B) {
	int pp;
	double alpha= 1;
	double * C = NULL;
	double beta = 0;
	double zero = 0;
	double one = 1;
	int n2;
	int INCX = 1;
	int INCY = 1;
	int INCZ = 0;
	int np1;
	n2 = n * n;
	C = (double *) MALLOC ( sizeof(double) * n2 );
	C2F(dcopy)(&n2,&zero,&INCZ,C,&INCX); // C := zeros
	pp = p;
	// Set the identity matrix in B
	C2F(dcopy)(&n2,&zero,&INCZ,B,&INCX); // B := zeros
	np1 = n + 1;
	C2F(dcopy)(&n,&one,&zero,B,&np1); // B := identity
	while ( pp > 0 )
	{
		if ( pp % 2 == 1 ) 
		{	
			C2F(dgemm)("n", "n", &n, &n, &n, &alpha, A, &n, B, &n, &beta, C, &n); // C := A * B
			C2F(dcopy)(&n2,C,&INCX,B,&INCY); // B := C
			pp = pp - 1;
		}
		C2F(dgemm)("n", "n", &n, &n, &n, &alpha, A, &n, A, &n, &beta, C, &n); // C := A * A
		C2F(dcopy)(&n2,C,&INCX,A,&INCY); // A := C
		pp = pp / 2;
	}
	FREE(C);
}


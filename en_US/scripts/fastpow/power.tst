// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//test with scalar vector and matrices

// REAL VECTORS
x1=1:3;
x0=0:2;
assert_equal ( x1^2 , [1 4 9] );
assert_equal ( x0^2 , [0 1 4] );
assert_equal ( x1^0 , [1 1 1] );

assert_close ( x1^(2*(1+%eps)) , x1^2 , 100*%eps  );
assert_close ( x0^(2*(1+%eps)) , x0^2 , 100*%eps  );

assert_close ( x1^(-2) , [1 0.25 1/9] , 100*%eps  );
assert_close ( x1^(-2*(1+%eps)) , [1 0.25 1/9] , 100*%eps  );

// COMPLEX CASE
p=2+%eps*%i;
assert_close ( x1^p , x1^2 , 100*%eps  );
assert_close ( x0^p , x0^2 , 100*%eps  );
assert_close ( x1^(-p) , x1^(-2) , 100*%eps  );

y=%eps*%eps*ones(1,3);
x1=x1+y;x0=x0+y;
assert_close ( x1^2 , [1 4 9] , 100*%eps  );
assert_close ( x0^2 , [0 1 4] , 100*%eps  );

x1^2.000000001;x0^2.000000001;
assert_close ( x1^2 , [1 4 9] , 100*%eps  );
assert_close ( x0^2 , [0 1 4] , 100*%eps  );

assert_close ( x1^(-2) , [1 0.25 1/9] , 100*%eps  );
assert_close ( x1^(-2*(1+%eps)) , [1 0.25 1/9] , 100*%eps  );
assert_close ( x1^p , x1^2 , 100*%eps  );
assert_close ( x0^p , x0^2 , 100*%eps  );
assert_close ( x1^(-p) , x1^(-2) , 100*%eps  );

// .^ TEST WITH  REAL VECTORS

x1=1:3;
x0=0:2;
assert_equal ( x1.^2 , [1 4 9]  );
assert_equal ( x0.^2 , [0 1 4]  );
assert_equal ( x1.^0 , [1 1 1]  );

assert_close ( x1.^(2*(1+%eps)) , x1.^2 , 100*%eps  );
assert_close ( x0.^(2*(1+%eps)) , x0.^2 , 100*%eps  );

assert_close ( x1.^(-2) , [1 0.25 1/9] , 100*%eps  );
assert_close ( x1.^(-2*(1+%eps)) , [1 0.25 1/9] , 100*%eps  );

// .^ TEST WITH  COMPLEX VECTORS

p=2+%eps*%i;
assert_close ( x1.^p , x1.^2 , 100*%eps  );
assert_close ( x0.^p , x0.^2 , 100*%eps  );
assert_close ( x1.^(-p) , x1.^(-2) , 100*%eps  );

y=%eps*%eps*ones(1,3);
x1=x1+y;x0=x0+y;
assert_close ( x1.^2 , [1 4 9] , 100*%eps  );
assert_close ( x0.^2 , [0 1 4] , 100*%eps  );

x1.^2.000000001;x0.^2.000000001;
assert_close ( x1.^2 , [1 4 9] , 100*%eps  );
assert_close ( x0.^2 , [0 1 4] , 100*%eps  );

assert_close ( x1.^(-2) , [1 0.25 1/9] , 100*%eps  );
assert_close ( x1.^(-2*(1+%eps)) , [1 0.25 1/9] , 100*%eps  );
assert_close ( x1.^p , x1.^2 , 100*%eps  );
assert_close ( x0.^p , x0.^2 , 100*%eps  );
assert_close ( x1.^(-p) , x1.^(-2) , 100*%eps  );
//
// EMPTY MATRIX
//=============
assert_equal ( []^1  ,  []  );
assert_equal ( []^[1 2 3]  ,  []  );
assert_equal ( [1 2 3]^[]  ,  []  );
assert_equal ( []^[]  ,  []  );

// SQUARE MATRIX
//==============

x1=[1 2;3 4];
assert_equal ( x1^1 , x1  );
assert_equal ( x1^(-1) , inv(x1)  );
assert_equal ( x1^2 , x1*x1  );
assert_equal ( x1^(-2) , inv(x1)^2  );

x1(1,1)=x1(1,1)+%i;
assert_equal ( x1^2 , x1*x1)  );
assert_equal ( x1^(-2) , inv(x1)^2)  );
assert_equal ( x1^1 , x1  );
assert_equal ( x1^(-1) , inv(x1)  );

assert_equal ( rand(4,4)^0 , eye(4,4))  );


x1=[1 2;2 3];
x2=x1^(1/2);
assert_close ( x2^(2) , x1 , 100*%eps  );
x2=x1^(-1/2);
assert_close ( x2^(-2) , x1 , 100*%eps  );

x1=[1 2+%i;2-%i 3];
x2=x1^(1/2);
assert_close ( x2^(2) , x1 , 100*%eps  );
x2=x1^(-1/2);
assert_close ( x2^(-2) , x1 , 100*%eps  );

//TEST WITH POLYNOMIAL VECTOR AND MATRICES
//---------------------------------------
s=poly(0,'s');
assert_equal ( coeff(s^3+1) , [1 0 0 1])  );


x1=[1 s+1 s^3+1];
assert_equal ( x1^2 , [1 1+2*s+s^2  1+2*s^3+s^6])  );
assert_equal ( coeff(x1^0) , [1 1 1])  );
assert_equal ( x1^3 , [1,1+3*s+3*s^2+s^3,1+3*s^3+3*s^6+s^9])  );
assert_equal ( (x1^(-1)-[1 1/(1+s)  1/(1+s^3)]) , 0)  );

x1=[1 s+1 s.^3+1];
assert_equal ( x1.^2 , [1 1+2*s+s.^2  1+2*s.^3+s.^6])  );
assert_equal ( coeff(x1.^0) , [1 1 1])  );
assert_equal ( x1.^3 , [1,1+3*s+3*s^2+s^3,1+3*s^3+3*s^6+s^9])  );
assert_equal ( (x1.^(-1)-[1 1/(1+s)  1/(1+s.^3)]) , 0)  );




x1=[s+1 2*s;3+4*s^2 4];
assert_equal ( x1^1 , x1)  );
assert_equal ( x1^(-1) , inv(x1))  );
assert_equal ( x1^2 , x1*x1)  );
assert_equal ( x1^(-2) , inv(x1)^2)  );

x1(1,1)=x1(1,1)+%i;
assert_equal ( x1^2 , x1*x1)  );
//assert_equal ( x1^(-2) , inv(x1)^2)  );  //simp complexe non implemented
assert_equal ( x1^1 , x1)  );
//assert_equal ( x1^(-1) , inv(x1))  ); //simp complexe non implemented




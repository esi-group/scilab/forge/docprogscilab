// Copyright (C) 2010 - Michael Baudin

// Some examples of advanced function topics.

//**************************************************
//
// Inquiring about functions

function y = myfunction ( x )
  y = 2 * x
endfunction
type(myfunction)
typeof(myfunction)
type(eye)
typeof(eye)

optimizationlib
get_function_path("derivative")
editor(get_function_path("derivative"))
typeof(derivative)

get_function_path("optim")
typeof(optim)

//**************************************************
//
// Functions are not reserved

function y = rand(t)
  y = t + 1
endfunction

rand(1)

//**************************************************
//
// Functions are variables

function y = f ( t )
  y = t + 1
endfunction

fp = f

fp ( 1 )

// Using a callback to compute the numerical derivative 
// of a function.
function y = myf ( x ) 
  y = x^2
endfunction
y = derivative ( myf , 2 )









// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// Examples of use of the feval function.

function f = myquadratic ( x )
    f = x(1)**2 + x(2)**2;
endfunction

// Two nested loops

function [x,y,z] = gendataSlow (n)
    x = linspace ( -1 , 1 , n );
    y = linspace ( -1 , 1 , n );
    for i = 1:n
        for j = 1:n
            z ( i , j ) = myquadratic ( [x(i),y(j)] );
        end
    end
endfunction

n = 100;
[x,y,z] = gendataSlow (n);

// An efficient example with feval

function f = myquadratic2 ( x1 , x2 )
    f = myquadratic ( [x1 x2] )
endfunction

function [x,y,z] = gendataFast (n)
    x = linspace ( -1 , 1 , n );
    y = linspace ( -1 , 1 , n );
    z = feval ( x , y , myquadratic2 );
endfunction

n = 100;
[x,y,z2] = gendataFast (n);

and(z2==z)

// Benchmarking
n = 200;
benchfun("Slow",gendataSlow,list(n),3,10);
benchfun("Fast",gendataFast,list(n),3,10);

// Create a graph with the scibench module
scibench_dynbenchfun ( %t , %t , 0.1 , 8 , 1.2 , gendataSlow , list() , 3 );
scibench_dynbenchfun ( %t , %t , 0.1 , 8 , 1.2 , gendataFast , list() , 3 );

// Conclusion: no real gain! This is weird...



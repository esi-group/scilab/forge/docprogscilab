// Copyright (C) 2010 - Michael Baudin

// A demo of a function which takes a variable number of input 
// arguments
function [fp,fpp] = myderivative1 ( f , x , order , h )
  if ( order == 1 ) then
    fp = (f(x+h) - f(x))/h                  // h=%eps^(1/2)
    fpp = (f(x+2*h) - 2*f(x+h) + f(x) )/h^2 // h=%eps^(1/3)
  else
    fp = (f(x+h) - f(x-h))/(2*h)          // h=%eps^(1/3)
    fpp = (f(x+h) - 2*f(x) + f(x-h) )/h^2 // h=%eps^(1/4)
  end
endfunction

// Check #1
function y = myfun ( x )
  y = cos(x)
endfunction

format("e",25)
x0 = %pi/6;
fp = myderivative1 ( myfun , x0 , 1 , %eps^(1/2) )
fp = myderivative1 ( myfun , x0 , 2 , %eps^(1/3) )


format("e",25)
x0 = %pi/6;
[fp,fpp] = myderivative1 ( myfun , x0 , 1 , %eps^(1/2) )
[fp,fpp] = myderivative1 ( myfun , x0 , 2 , %eps^(1/3) )

// Check #2
function y = myfun2 ( x )
  y = x^4
endfunction

x0 = 1;
order = 1;
h = %eps^(1/2);
fp = myderivative1 ( myfun2 , x0 , order , h )
[fp,fpp] = myderivative1 ( myfun2 , x0 , order , h )
//
h = %eps^(1/3);
order = 2;
[fp,fpp] = myderivative1 ( myfun2 , x0 , order , h )

function varargout = myderivative2 ( varargin )
  [lhs ,rhs ]=argn()
  if ( rhs < 2 | rhs > 4 ) then
    error ( msprintf(..
      "%s: Expected from %d to %d input arguments, but %d are provided",..
      "myderivative2",2,4,rhs))
  end
  if ( lhs > 2 ) then
    error ( msprintf(..
      "%s: Expected from %d to %d output arguments, but %d are provided",..
      "myderivative2",0,2,lhs))
  end
  f = varargin(1)
  x = varargin(2)
  if ( rhs >= 3 ) then
    order = varargin(3)
  else
    order = 2
  end
  if ( rhs >= 4 ) then
    h = varargin(4)
    hflag = %t
  else
    hflag = %f
  end
  // Compute f'
  if ( order == 1 ) then
    if ( ~hflag ) then
      h = %eps^(1/2)
    end
    fp = (f(x+h) - f(x))/h
  else
    if ( ~hflag ) then
      h = %eps^(1/3)
    end
    fp = (f(x+h) - f(x-h))/(2*h)
  end
  varargout(1) = fp
  if ( lhs >= 2 )  then
    // Compute f''
    if ( order == 1 ) then
      if ( ~hflag ) then
        h = %eps^(1/3)
      end
      fpp = (f(x+2*h) - 2*f(x+h) + f(x) )/(h^2)
    else
      if ( ~hflag ) then
        h = %eps^(1/4)
      end
      fpp = (f(x+h) - 2*f(x) + f(x-h) )/(h^2)
    end
    varargout(2) = fpp
  end
endfunction

// Calling sequences which work
format("e",25)
x0 = %pi/6;
fp = myderivative2 ( myfun , x0 )

fp = myderivative2 ( myfun , x0 , 1 )
[fp,fpp] = myderivative2 ( myfun , x0 , order )
// Calling sequences which do NOT work
df = myderivative2 ( myfun )

// Show with 2 output arguments
[df,dy] = myderivative2 ( myfun , x0 )

// Test on another point
[fp,fpp] = myderivative2 ( myfun , %pi/6 , 1 )
[fp,fpp] = myderivative2 ( myfun , %pi/6 , 2 )


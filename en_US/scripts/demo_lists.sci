// Copyright (C) 2009 - Michael Baudin


// Put an integer, a string and a matrix in a list
myflint = 12;
mystr = "foo";
mymatrix = [1 2 3 4];
mylist = list ( myflint , mystr , mymatrix )
//
size(mylist)
//
mylist(1)
//
[s,m] = mylist(2:3)
//
mylist(3)(4)
mylist(3)(4) = 12
//
// Two ways to make a loop.
// The bad
for i = 1:size(mylist)
  e = mylist(i);
  mprintf("Element #%d: type=%s.\n",i,typeof(e))
end
//
// The good
for e = mylist
  mprintf("Type=%s.\n",typeof(e))
end
//
// Inserting at the end
mylist = list();
mylist($+1) = 12;
mylist($+1) = "foo";
mylist($+1) = [1 2 3 4];


// A practical use for lists

function [f,g,ind] = cost ( x , ind )
  f = 4 * x(1)^2 + 5 * x(2)^2 + 6 + 7*(x(3)-1)^2
  g = [
    8 * x(1)
    10 * x(2)
    14 * (x(3)-1)
    ]
endfunction

x0 = [1 2 3]';
[f,g,ind] = cost ( x0 , 1 )
[fopt,xopt] = optim ( cost , x0 )
// Using derivative to check the gradient.
derivative(list(cost,1),x0)'
//
// Passing arguments as a list

function [f,g,ind] = cost2 ( x , p1 , p2 , p3 , p4 , ind )
  f = p1 * x(1)^2 + p2 * x(2)^2 + p3 + p4 * (x(3)-1)^2
  g = [
    2*p1 * x(1)
    2*p2 * x(2)
    2*p4 * (x(3)-1)
    ]
endfunction

x0 = [1 2 3]';
p1 = 4;
p2 = 5;
p3 = 6;
p4 = 7;
[f,g,ind] = cost2 ( x0 , p1 , p2 , p3 , p4 )
derivative(list(cost2,p1,p2,p3,p4,1),x0)'
[fopt,xopt] = optim ( list(cost2,p1,p2,p3,p4) , x0 )



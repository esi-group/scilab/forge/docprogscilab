// Copyright (C) 2010 - Michael Baudin

// Check that function works
//A = testmatrix("frk",n);
n = 10;
A = grand(n,n,"def");
log10(cond(A))
e = ones(n,1);
b = A * e;
x = gausspivotalnaive ( A , b );
norm(e-x)/norm(e)

// Profile this
add_profiling("gausspivotalnaive")
x = gausspivotalnaive ( A , b );
//showprofile(gausspivotalnaive)
plotprofile(gausspivotalnaive)

//
// Check less naive implementation
//
x = gausspivotalnaive ( A , b );
norm(e-x)/norm(e)
//
add_profiling("gausspivotal")
x = gausspivotal ( A , b );
plotprofile(gausspivotal)

// Compare the performances of the two algorithms
stacksize("max");
n = 100;
A = grand(n,n,"def");
e = ones(n,1);
b = A * e;
benchfun ( "naive" , gausspivotalnaive , list( A , b ) , 1 , 10 );
benchfun ( "fast" , gausspivotal , list( A , b ) , 1 , 10 );




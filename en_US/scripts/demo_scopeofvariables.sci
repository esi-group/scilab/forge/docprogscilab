// Copyright (C) 2010 - Michael Baudin

// Demonstration of the scope of variables
// through the call stack.

// At one level
function y = f ( x )
  y = a * x
endfunction

a = 2;
x = 3;
y = f(x)

// At two levels
function y = f ( x )
  y = mysubfunction ( x )
endfunction
function y = mysubfunction ( x )
  y = a * x
endfunction

a = 2;
x = 3;
y = f(x)



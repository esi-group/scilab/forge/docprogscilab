// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//////////////////////////////////////////////////////////
// The purpose of this example is to show why using the 
// key=value to provide optional arguments is sometimes a 
// poor programming practice.
// It can lead to difficulties to produce new releases of the 
// software and can lead to difficult bugs.
// A safer approach, based on the empty matrix, is suggested.

//////////////////////////////////////////////////////////
// A simple function.
function y = myfunction1 ( x, a, b, c )
  y = a*x^2+b*x+c
endfunction
myfunction1 ( 1, 1, 3, 1)

//////////////////////////////////////////////////////////
// A function with optional arguments managed with "exists".
function y = myfunction2 ( x, a, b, c )
  if ~exists("a","local") then
    a = 1
  end
  if ~exists("b","local") then
    b = 1
  end
  if ~exists("c","local") then
    c = 1
  end
  y = a*x^2+b*x+c
endfunction
myfunction2 ( 1 )
myfunction2 ( 1, b=3 )

// Advantages:
// * The argument is named, which allows to "skip" the argument a, 
//   which comes before in the calling sequence.
// Drawbacks:
// * We cannot change the name of the arguments.
//   In all the future releases of this function, the optional 
//   arguments must be "a", "b" and "c".
// * This can lead to bugs.

//////////////////////////////////////////////////////////
// The user uses a wrong variable name, which is ignored.
myfunction2 ( 1, d=3 )
// The variable is just ignored, but the user does not 
// get an error message: this silently fails.
// The previous example is simple, but suppose that the 
// variable name is "H_form", but the user writes "Hform":
// would someone notice the bug ?


//////////////////////////////////////////////////////////
// A correct implementation

// This is a simplified implementation of apifun_argindefault.

// If the argument ivar is not present in vararglist, returns default.
// If the argument ivar is present in vararglist : 
// * if the argument is [], returns default.
// * else return vararglist(ivar).
function argin = argindefault ( vararglist , ivar , default )
    rhs = length(vararglist)
    if ( rhs < ivar ) then
        argin = default
    else
        if ( typeof(vararglist(ivar))== "constant" ) then
            if ( vararglist(ivar) <> [] ) then
                argin = vararglist(ivar)
            else
                argin = default
            end
        else
            argin = vararglist(ivar)
        end
    end
endfunction

// Calling sequence
// y = myfunction4 ( x)
// y = myfunction4 ( x, a)
// y = myfunction4 ( x, a, b)
// y = myfunction4 ( x, a, b, c)
// Description
// Any optional argument is replaced by its default value.
function y = myfunction4 ( varargin )
	[lhs,rhs]=argn();
	if rhs<1 | rhs>3 then
		lclmsg = "%s: Wrong number of input arguments: %d to %d expected.\n"
		error(msprintf(gettext(lclmsg),"myfunction4",1,3));
	end
	x = varargin(1)
	//
	a = argindefault ( varargin , 2 , 1 )
	b = argindefault ( varargin , 3 , 1 )
	c = argindefault ( varargin , 4 , 1 )
	//
  y = a*x^2+b*x+c
endfunction
myfunction4 ( 1 )
myfunction4 ( 1, [], 2)

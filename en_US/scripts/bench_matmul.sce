// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Benchmarking the matrix-matrix product

// References
// "Programming in Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docprogscilab/downloads/

/////////////////////////////////////////////////////////////////
//
// A basic benchmark
if ( %f ) then
stacksize("max");
rand( "normal" );
n = 1000;
A = rand(n,n);
B = rand(n,n);
tic();
C = A * B;
t = toc();
mflops = 2*n^3/t/1.e6;
disp([n t mflops])
end

/////////////////////////////////////////////////////////////////
//
// A subtle benchmark
scf();

stacksize("max");
rand( "normal" );
lines(0);

s = stacksize();
MB = round(s(1)*8/10^6);
nmax = sqrt(s(1));
mprintf("Memory: %d (MB)\n",MB);
mprintf("Maximum n: %d\n",nmax);

timemin = 0.1;
timemax = 8.0;
nfact = 1.2;

//
// Make a loop over n
xtitle("Matrix-Matrix multiply","Matrix Order (n)","Megaflops");
n = 1;
k = 1;
perftable = [];
while ( %t )
  A = rand(n,n);
  B = rand(n,n);
  tic();
  ierr = execstr("C = A * B","errcatch");
  t = toc();
  if ( ierr <> 0 ) then
    laerr = lasterror();
    disp("Error:");
    disp(laerr);
    break
  end
  if ( t > timemin ) then
    mflops = 2*n^3/t/1.e6;
    perftable(k,:) = [n t mflops];
    plot(n,mflops,"bo-")
    mprintf("Run #%d: n=%6d, T=%.3f (s), Mflops=%6d\n",k,perftable(k,1),perftable(k,2),perftable(k,3))
    k = k+1;
  end
  if ( t > timemax ) then
    break
  end
  n = ceil(nfact * n);
end
// Search for best performance
[M,k] = max(perftable(:,3));
mprintf("Best performance:")
mprintf(" N=%d, T=%.3f (s), MFLOPS=%d\n",perftable(k,1),perftable(k,2),perftable(k,3));



/////////////////////////////////////////////////////////////////
//
// A pre-stored benchmark
// Scilab v5.2.2 on Windows XP 32 bits. 
// CPU : AMD Athlon 3200+ at 2 GHz 
// 1 GB of memory. 

if ( %f ) then
perfRefBlas = [
    308,0.141,414;   
    370,0.31,326;
    444,0.411,425;
    533,0.691,438;
    640,1.241,422;
    768,2.023,447;
    922,3.425,457;
    1107,5.918,458;
    1329,10.215,459
];
perfMKL = [
    533,0.16,1892;                 
    640,0.27100000000000002,1934;
    768,0.43099999999999994,2102;
    922,0.731,2144;
    1107,1.28200000000000003,2116;
    1329,2.1339999999999999,2199;
    1595,3.665,2214;
    1914,6.21900000000000031,2254;
    2297,10.7959999999999994,2245
];
perfATLAS = [
    533,0.16,1892;    
    640,0.26,2016;
    768,0.441,2054;
    922,0.751,2087;
    1107,1.312,2067;
    1329,2.223,2111;
    1595,3.766,2154;
    1914,6.599,2125;
    2297,12.328,1966
 ];
xtitle("Matrix-Matrix multiply","Matrix Order (n)","Megaflops");
plot(perfRefBlas(:,1),perfRefBlas(:,3),"bo-")
plot(perfMKL(:,1),perfMKL(:,3),"r*-")
plot(perfATLAS(:,1),perfATLAS(:,3),"gx-")
legend ( ["Reference BLAS" "Intel MKL" "ATLAS" ] );
h = gcf();
h.children.children(1).legend_location = "in_upper_left";
end

if ( %f ) then
// A pre-stored benchmark
// scilab-5.3.0-beta-4 on Linux Ubuntu 32 bits. 
// CPU : Intel(R) Pentium(R) M processor 2.00GHz at 2 GHz 
// 1 GB of memory. 
// cache size	: 2048 KB
// fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov clflush dts acpi mmx fxsr sse sse2 ss tm pbe nx up bts est tm2
perfATLAS = [
    410.18627    0.104007    1327.1218  
    492.22352    0.156009    1528.859  
    590.66823    0.256016    1609.8808  
    708.80187    0.444028    1603.9625  
    850.56225    0.752047    1636.4522  
    1020.6747    1.296081    1640.8161  
    1224.8096    2.22814    1649.2759  
    1469.7716    3.760235    1688.7468  
    1763.7259    6.3924    1716.5613  
    2116.4711    11.224702    1689.2432  
];

perfRefBlas = [
    341.82189    0.104006    768.01769  
    410.18627    0.176011    784.21211  
    492.22352    0.368023    648.10016  
    590.66823    0.728045    566.11232  
    708.80187    1.420089    501.52086  
    850.56225    2.428152    506.84181  
    1020.6747    4.316269    492.70111  
    1224.8096    7.432464    494.4279  
    1469.7716    12.732796    498.7188  
];
xtitle("Matrix-Matrix multiply","Matrix Order (n)","Megaflops");
plot(perfRefBlas(:,1),perfRefBlas(:,3),"bo-")
plot(perfATLAS(:,1),perfATLAS(:,3),"gx-")
legend ( ["Reference BLAS" "ATLAS" ] );
h = gcf();
h.children.children(1).legend_location = "in_upper_left";

end


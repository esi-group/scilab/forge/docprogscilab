// Copyright (C) 2010 - Michael Baudin

// Using strings every day

// Conversion to a string
x = [1 2 3 4 5];
str = string(x)
typeof(str)
size(str)

// Ne marche pas
function [y1, y2] = myfun ( x1 , x2 , x3 )
  y1 = x1 + x2
  y2 = x3
endfunction
[out,in,text]=string(myfun)

// sci2exp
x = [1 2 3 4 5];
str = sci2exp(x)
size(str)


A = [
  1 2 3 
  4 5 6];
sci2exp(A)

// strcat(string(x)," ")
x = [1 2 3 4 5];
strcat(string(x)," ")
mprintf("x=[%s]\n",strcat(string(x)," "))

// classes of strings
isdigit("0")
isdigit("12")
isdigit("d3s4")


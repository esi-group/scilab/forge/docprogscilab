// Copyright (C) 2010 - Michael Baudin

// Two sequences converging toward the Golden ratio : (1+sqrt(5))/2 = 1.61803398874989484820458683436563811772...

// A : v = (u+a)/u,           u(0) = 1, a = 1
// B : v = (u^2+1)/(b*u-1),   u(0) = 2, b = 2
// C : v = (u^2+c*u)/(u^2+1), u(0) = 3, c = 2

// First design : 
// S = init ( name ), 
// [this,v] = next ( this )

function S = init (name)
  S = tlist(["SEQ","name","u","n","a","b","c"])
  S.n = 0
  if ( name == "A" ) then
    S.u = 1
    S.a = 1
  elseif ( name == "B" ) then
    S.u = 2
    S.b = 2
  elseif ( name == "C" ) then
    S.u = 3
    S.c = 2
  else
    error ( msprintf("%s: Unknown name %s","init",name) )
  end
  S.name = name
endfunction

function %SEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %SEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence")
  k = k + 1
  str(k) = sprintf("========")
  k = k + 1
  str(k) = sprintf("name: %s\n", string(this.name))
  k = k + 1
  str(k) = sprintf("n: %s\n", string(this.n))
  k = k + 1
  if ( this.name == "A" ) then
    str(k) = sprintf("a: %s\n", string(this.a))
    k = k + 1
  elseif ( this.name == "A" ) then
    str(k) = sprintf("b: %s\n", string(this.b))
    k = k + 1
  elseif ( this.name == "C" ) then
    str(k) = sprintf("c: %s\n", string(this.c))
    k = k + 1
  else
    error ( msprintf("%s: Unknown name %s","%SEQ_string",name) )
  end
  str(k) = sprintf("u: %s\n", string(this.u))
  k = k + 1
endfunction

function [this,v] = next ( this )
  u = this.u
  if ( this.name == "A" ) then
    v = (u + this.a)/u
  elseif ( this.name == "B" ) then
    v = (u^2 + 1)/(this.b * u - 1)
  elseif ( this.name == "C" ) then
    v = (u^2 + this.c * u)/(u^2 + 1)
  else
    error ( "Unknown name" )
  end
  this.n = 1 + this.n
  this.u = v
endfunction

S = init("A");
[S,v] = next(S)
[S,v] = next(S)
[S,v] = next(S)

SA = init("A");
SB = init("B");
SC = init("C");
for i = 1 : 20
  [SA,vA] = next(SA);
  [SB,vB] = next(SB);
  [SC,vC] = next(SC);
  mprintf("u(%3d)=%.17e, %.17e, %.17e\n", i, vA, vB, vC)
end

/////////////////////////////////////////////////////////////////////

// Second design
// S = A_init(), [this,v] = A_next ( this )
// S = B_init(), [this,v] = B_next ( this )
// S = C_init(), [this,v] = C_next ( this )
// S = init ( name ), next ( this )

//
// Class A
//

function S = A_init ()
  S = tlist(["ASEQ","u","n","a"])
  S.n = 0
  S.u = 1
  S.a = 1
endfunction

function %ASEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %ASEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence A")
  k = k + 1
  str(k) = sprintf("==========")
  k = k + 1
  str(k) = sprintf("n: %s\n", string(this.n))
  k = k + 1
  str(k) = sprintf("a: %s\n", string(this.a))
  k = k + 1
  str(k) = sprintf("u: %s\n", string(this.u))
  k = k + 1
endfunction

function [this,v] = A_next ( this )
  u = this.u
  v = (u + this.a)/u
  this.n = 1 + this.n
  this.u = v
endfunction

//
// Class B
//
function S = B_init (name)
  S = tlist(["BSEQ","u","n","b"])
  S.n = 0
  S.u = 2
  S.b = 2
endfunction

function %BSEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %BSEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence B")
  k = k + 1
  str(k) = sprintf("==========")
  k = k + 1
  str(k) = sprintf("n: %s\n", string(this.n))
  k = k + 1
  str(k) = sprintf("b: %s\n", string(this.b))
  k = k + 1
  str(k) = sprintf("u: %s\n", string(this.u))
  k = k + 1
endfunction

function [this,v] = B_next ( this )
  u = this.u
  v = (u^2 + 1)/(this.b * u - 1)
  this.n = 1 + this.n
  this.u = v
endfunction
//
// Class C
//
function S = C_init (name)
  S = tlist(["CSEQ","u","n","c"])
  S.n = 0
  S.u = 3
  S.c = 2
endfunction

function %CSEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %CSEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence C")
  k = k + 1
  str(k) = sprintf("==========")
  k = k + 1
  str(k) = sprintf("n: %s\n", string(this.n))
  k = k + 1
  str(k) = sprintf("c: %s\n", string(this.c))
  k = k + 1
  str(k) = sprintf("u: %s\n", string(this.u))
  k = k + 1
endfunction

function [this,v] = C_next ( this )
  u = this.u
  v = (u^2 + this.c * u)/(u^2 + 1)
  this.n = 1 + this.n
  this.u = v
endfunction
//
// Driver Class
//
function S = init (name)
  S = tlist(["SEQ","name","sequence"])
  if ( name == "A" ) then
    S.sequence = A_init()
  elseif ( name == "B" ) then
    S.sequence = B_init()
  elseif ( name == "C" ) then
    S.sequence = C_init()
  else
    error ( msprintf("%s: Unknown name %s","init",name) )
  end
  S.name = name
endfunction

function %SEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %SEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence")
  k = k + 1
  str(k) = sprintf("========")
  k = k + 1
  str(k) = sprintf("name: %s\n", string(this.name))
  k = k + 1
  strseq = string(this.sequence)
  nbrows = size(strseq,"r")
  for i = 1 : nbrows
    str(k) = strseq ( i )
    k = k + 1
  end
endfunction

function [this,v] = next ( this )
  if ( this.name == "A" ) then
    [this.sequence,v] = A_next ( this.sequence )
  elseif ( this.name == "B" ) then
    [this.sequence,v] = B_next ( this.sequence )
  elseif ( this.name == "C" ) then
    [this.sequence,v] = C_next ( this.sequence )
  else
    error ( "Unknown name" )
  end
endfunction

S = init("A");
[S,v] = next(S)
[S,v] = next(S)
[S,v] = next(S)

SA = init("A");
SB = init("B");
SC = init("C");
for i = 1 : 20
  [SA,vA] = next(SA);
  [SB,vB] = next(SB);
  [SC,vC] = next(SC);
  mprintf("u(%3d)=%.17e, %.17e, %.17e\n", i, vA, vB, vC)
end

/////////////////////////////////////////////////////////////////////

// Third design
// S = abstract_init (u0), [this,v] = abstract_next ( this )
// S = A_init(), [this,v] = A_next ( this )
// S = B_init(), [this,v] = B_next ( this )
// S = C_init(), [this,v] = C_next ( this )
// S = init ( name ), next ( this )

//
// Abstract Class
//

function S = abstract_init ( u0 )
  S = tlist(["ABSSEQ","u","n"])
  S.n = 0
  S.u = u0
endfunction

function %ABSSEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %ABSSEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence Abstract")
  k = k + 1
  str(k) = sprintf("=================")
  k = k + 1
  str(k) = sprintf("n: %s\n", string(this.n))
  k = k + 1
  str(k) = sprintf("u: %s\n", string(this.u))
  k = k + 1
endfunction

function this = ABS_next ( this , v )
  this.n = 1 + this.n
  this.u = v
endfunction

//
// Class A
//

function S = A_init ()
  S = tlist(["ASEQ","abstract","a"])
  S.abstract = abstract_init ( 1 )
  S.a = 1
endfunction

function %ASEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %ASEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence A")
  k = k + 1
  str(k) = sprintf("==========")
  k = k + 1
  str(k) = sprintf("a: %s\n", string(this.a))
  k = k + 1
  strabs = string(this.abstract)
  nbrows = size(strabs,"r")
  for i = 1 : nbrows
    str(k) = strabs ( i )
    k = k + 1
  end
endfunction

function [this,v] = A_next ( this )
  u = this.abstract.u
  v = (u + this.a)/u
  this.abstract = ABS_next ( this.abstract , v )
endfunction

//
// Class B
//
function S = B_init (name)
  S = tlist(["BSEQ","abstract","b"])
  S.abstract = abstract_init ( 2 )
  S.b = 2
endfunction

function %BSEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %BSEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence B")
  k = k + 1
  str(k) = sprintf("==========")
  k = k + 1
  str(k) = sprintf("b: %s\n", string(this.b))
  k = k + 1
  strabs = string(this.abstract)
  nbrows = size(strabs,"r")
  for i = 1 : nbrows
    str(k) = strabs ( i )
    k = k + 1
  end
endfunction

function [this,v] = B_next ( this )
  u = this.abstract.u
  v = (u^2 + 1)/(this.b * u - 1)
  this.abstract = ABS_next ( this.abstract , v )
endfunction
//
// Class C
//
function S = C_init (name)
  S = tlist(["CSEQ","abstract","c"])
  S.abstract = abstract_init ( 3 )
  S.c = 2
endfunction

function %CSEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %CSEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence C")
  k = k + 1
  str(k) = sprintf("==========")
  k = k + 1
  str(k) = sprintf("c: %s\n", string(this.c))
  k = k + 1
  strabs = string(this.abstract)
  nbrows = size(strabs,"r")
  for i = 1 : nbrows
    str(k) = strabs ( i )
    k = k + 1
  end
endfunction

function [this,v] = C_next ( this )
  u = this.abstract.u
  v = (u^2 + this.c * u)/(u^2 + 1)
  this.abstract = ABS_next ( this.abstract , v )
endfunction
//
// Driver Class
//
function S = init (name)
  S = tlist(["SEQ","name","sequence"])
  if ( name == "A" ) then
    S.sequence = A_init()
  elseif ( name == "B" ) then
    S.sequence = B_init()
  elseif ( name == "C" ) then
    S.sequence = C_init()
  else
    error ( msprintf("%s: Unknown name %s","init",name) )
  end
  S.name = name
endfunction

function %SEQ_p ( this )
  str = string(this)
  nbrows = size(str,"r")
  for i = 1 : nbrows
    mprintf("%s\n",str(i))
  end
endfunction

function str = %SEQ_string ( this )
  str = []
  k = 1
  str(k) = sprintf("Sequence")
  k = k + 1
  str(k) = sprintf("========")
  k = k + 1
  str(k) = sprintf("name: %s\n", string(this.name))
  k = k + 1
  strseq = string(this.sequence)
  nbrows = size(strseq,"r")
  for i = 1 : nbrows
    str(k) = strseq ( i )
    k = k + 1
  end
endfunction

function [this,v] = next ( this )
  if ( this.name == "A" ) then
    [this.sequence,v] = A_next ( this.sequence )
  elseif ( this.name == "B" ) then
    [this.sequence,v] = B_next ( this.sequence )
  elseif ( this.name == "C" ) then
    [this.sequence,v] = C_next ( this.sequence )
  else
    error ( "Unknown name" )
  end
endfunction

S = init("A");
[S,v] = next(S)
[S,v] = next(S)
[S,v] = next(S)

SA = init("A");
SB = init("B");
SC = init("C");
for i = 1 : 20
  [SA,vA] = next(SA);
  [SB,vB] = next(SB);
  [SC,vC] = next(SC);
  mprintf("u(%3d)=%.17e, %.17e, %.17e\n", i, vA, vB, vC)
end


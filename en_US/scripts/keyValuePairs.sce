// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Manage (key,value) pairs

// Bibliography
// "Why using key=value syntax to manage input arguments is not a good idea"
// Michael Baudin, 2011
// http://wiki.scilab.org/


// options = keyValuePairs (default)
// options = keyValuePairs (default,key1,value1)
// options = keyValuePairs (default,key1,value1,key2,value2,...)
function options = keyValuePairs (default,varargin)
    [lhs,rhs]=argn();
   if rhs<1 then
     error(msprintf(gettext("%s: Wrong number of input arguments: at least %d expected.\n"),"derivative",1));
   end
    //
    if ( typeof(default) <> "st" ) then
        errmsg = msprintf(gettext("%s: Wrong type for argument %d: Struct expected.\n"), "keyValuePairs",1);
        error(errmsg)
    end
    if modulo(rhs,2)<>1 then
        errmsg = msprintf(gettext("%s: Even number of arguments."), "keyValuePairs");
        error(errmsg)
    end
    //
    nbkeys = length(varargin)/2;
    ivar = 0;
    options = default
    defaultnames = fieldnames(default)
    for i=1:nbkeys
        //
        // 1. Get the (key,value) pair
        ivar = ivar + 1;
        key = varargin(ivar);
        ivar = ivar + 1;
        // Use funcprot to enable the set of a function into the variable "value".
        // If not, a warning message is triggered, when a double value 
        // is stored into "value" after a function has already been 
        // stored in it.
        if ( i>1 & typeof(value)=="function") then
            prot = funcprot();
            funcprot(0);
            funcmode = %t
        else
            funcmode = %f
        end
        value = varargin(ivar);
        if ( funcmode ) then
            funcprot(prot);
        end
        //
        // 2. Check that the key exists
        i = find(key==defaultnames)
        if ( i == [] ) then
            lclmsg = "%s: Unknown key: %s."
            errmsg = msprintf(gettext(lclmsg), "keyValuePairs",key);
            error(errmsg)
        end
        //
        // 3. Process the key
        instr = "options."+key+"=value"
        ierr = execstr(instr,"errcatch")
        if (ierr<>0) then
            lamsg = lasterror()
            lclmsg = "%s: Error while setting the field %s: %s."
            errmsg = msprintf(gettext(lclmsg), "keyValuePairs",key,lamsg);
            error(errmsg)
        end
    end
endfunction

//
// Set the defaults
default.a = 1;
default.b = 1;
default.c = 1;
//
// Check with doubles
options = keyValuePairs (default)
expected.a=1;
expected.b=1;
expected.c=1;
//
options = keyValuePairs (default,"a",2)
expected.a=2;
expected.b=1;
expected.c=1;
//
options = keyValuePairs (default,"b",12)
expected.a=1;
expected.b=12;
expected.c=1;
//
options = keyValuePairs (default,"b",12,"a",999)
expected.a=999;
expected.b=12;
expected.c=1;
//
options = keyValuePairs (default,"c",-1,"b",12,"a",999)
expected.a=999;
expected.b=12;
expected.c=-1;

//
// Check with functions
function foo()
endfunction
function faa()
endfunction
function fii()
endfunction
//
// Set the defaults
default.a = 1;
default.b = 1;
default.c = [];
options = keyValuePairs (default,"c",foo,"b",12,"a",999)
expected.a=999;
expected.b=12;
expected.c=foo;
//
options = keyValuePairs (default,"c",foo,"b",faa,"a",999)
expected.a=999;
expected.b=faa;
expected.c=foo;
//
options = keyValuePairs (default,"c",foo,"b",faa,"a",fii)
expected.a=fii;
expected.b=faa;
expected.c=foo;

//
// Incremental method
default.a = 1;
default.b = 1;
default.c = [];
//
options = keyValuePairs (default)
expected.a=1;
expected.b=1;
expected.c=[];
//
options = keyValuePairs (options,"c",foo)
expected.a=1;
expected.b=1;
expected.c=foo;
//
options = keyValuePairs (options,"b",faa)
expected.a=1;
expected.b=faa;
expected.c=foo;
//
options = keyValuePairs (options,"a",2)
expected.a=2;
expected.b=faa;
expected.c=foo;
//
options = keyValuePairs (options,"c",foo)
expected.a=2;
expected.b=faa;
expected.c=foo;

// y = myfunction(x)
// y = myfunction(x,key1,value1,...)
// Available keys: 
// "a" (default a=1)
// "b" (default b=1)
// "c" (default c=1)
function y = myfunction(x,varargin)
    //
    // 1. Set the defaults
    default.a = 1
    default.b = 1
    default.c = 1
    //
    // 2. Manage (key,value) pairs
    options = keyValuePairs (default,varargin(1:$))
    //
    // 3. Get parameters
    a = options.a
    b = options.b
    c = options.c
    // TODO : check the types, size and content of a, b, c
    y = a*x^2+b*x+c
endfunction

y = myfunction(1)
expected = 1*1^2+1*1+1;
//
y = myfunction(1,"a",2)
expected = 2*1^2+1*1+1;
//
y = myfunction(1,"b",3)
expected = 1*1^2+3*1+1;
//
y = myfunction(1,"c",4,"b",3)
expected = 1*1^2+3*1+4;
//

// Copyright (C) 2010 - Michael Baudin

// A demo of calling back a function which may have 
// additionnal arguments using a list

function fp = myderivative1 ( __myderivative_f__ , x , h )
  fp = (__myderivative_f__(x+h) - __myderivative_f__(x))/h
endfunction

// Check #1
function y = myfun ( x )
  y = cos(1+2*x+3*x^2)
endfunction

format("e",25)
x = %pi/6;
h = %eps^(1/2);
fp = myderivative1 ( myfun , x , h )
expected = -sin(1+2*x+3*x^2) * (2+6*x)


expected = -sin(x)
fp = myderivative1 ( cos , x , h )

// A function with an additionnal argument
function y = myfun2 ( x , a )
  y = cos(a(1)+a(2)*x+a(3)*x^2)
endfunction

function fp = myderivative2 ( __myderivative_f__ , x , h )
  tyfun = typeof(__myderivative_f__)
  if ( and(tyfun<>["list" "function" "fptr"]) ) then
    error ( msprintf("%s: Unknown function type: %s",..
      "myderivative2",tyfun))
  end
  if ( tyfun == "list" ) then
    nitems = length(__myderivative_f__)
    if ( nitems<2 ) then
      error ( msprintf("%s: Too few elements in list: %d",..
        "myderivative2",nitems))
    end
    __myderivative_f__fun__ = __myderivative_f__(1)
    tyfun = typeof(__myderivative_f__fun__)
    if ( and(tyfun<>["function" "fptr"]) ) then
      error ( msprintf("%s: Unknown function type: %s",..
        "myderivative2",tyfun))
    end
    fxph = __myderivative_f__fun__ ( x + h , __myderivative_f__(2:$))
    fx = __myderivative_f__fun__ ( x , __myderivative_f__(2:$))
  else
    fxph = __myderivative_f__ ( x + h )
    fx = __myderivative_f__ ( x )
  end
  fp = (fxph - fx)/h
endfunction

x = %pi/6;
a = [1 2 3];
h = %eps^(1/2);
fp = myderivative2 ( list(myfun2,a) , x , h )
expected = -sin(a(1)+a(2)*x+a(3)*x^2) * (a(2)+2*a(3)*x)
//
fp = myderivative2 ( cos , x , h )
expected = -sin(x)
//
fp = myderivative2 ( myfun , x , h )
expected = -sin(x)
//
// Error case
myderivative2 ( "myfun" , x , h )


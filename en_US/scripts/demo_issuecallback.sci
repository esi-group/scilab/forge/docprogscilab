// Copyright (C) 2010 - Michael Baudin

////////////////////////////////////////////////////////////////////

// At the developer level
function y = myalgorithm ( x , f )
  y = f(x)
endfunction

// At the user level
function y = myfunction ( x )
  y = f ( x )
endfunction
function y = f ( x )
  y = x(1)^2 + x(2)^2
endfunction

// Use case
x = [1 2];
y = myalgorithm ( x , myfunction )
//  !--error 26 
// Too complex recursion! (recursion tables are full)
// at line       2 of function myfunction called by :
// at line       2 of function myfunction called by :
// at line       2 of function myfunction called by :
// ...

// The cause : when f is called in my algorithm, 
// it was allready defined, at a upper calling level, in 
// myfunction.
// Therefore, the function f, originally defined by the 
// user is not known anymore.
// The parser only knows that it is a function, and 
// calls it recursively, again, and again, and again.
////////////////////////////////////////////////////////////////////
// Another case of failure.

// At the developer level
function y = myalgorithm ( x , userf )
  // Set the presumably local variable f
  f = 1
  y = userf(x)
endfunction

// At the user level
function y = myfunction ( x )
  y = f ( x )
endfunction
function y = f ( x )
  y = x(1)^2 + x(2)^2
endfunction

// Use case
x = [1 2];
y = myalgorithm ( x , myfunction )
expected = x(1)^2 + x(2)^2
//  !--error 21 
//Invalid index.
//at line       2 of function myfunction called by :  
//at line       3 of function myalgorithm called by :  
//y = myalgorithm ( x , myfunction )
 
////////////////////////////////////////////////////////////////////

// Workaround #1 : at the user level
function y = myfunction ( x )
  function y = f ( x )
    y = x(1)^2 + x(2)^2
  endfunction
  y = f ( x )
endfunction

x = [1 2];
y = myalgorithm ( x , myfunction )
////////////////////////////////////////////////////////////////////

// Workaround #2 : at the developper level
function y = myalgorithm ( x , __myalgorithm_f__ )
  y = __myalgorithm_f__(x)
endfunction
x = [1 2];
y = myalgorithm ( x , myfunction )


// Copyright (C) 2010 - Michael Baudin

/////////////////////////////////////////////////////
// A demo of the execstr function

s = "foo"
instr = "disp(""" + s + """)"
execstr(instr)
//
x = 1
y = 2
instr = "z="+string(x)+"+"+string(y)
execstr(instr)
z
//
data = list();
for k = 1 : 2
  instr = "t = read(""datafile" + string(k) + ".txt"",2,3)"
  execstr(instr)
  data($+1)=t
end


/////////////////////////////////////////////////////
// A demo of the deff function

header = "y=myfun(x)"
body = [
"y(1) = 2*x(1)+x(2)-x(3)^2"
"y(2) = 2*x(2) + x(3)"
]
deff(header,body)
//
x = [1 2 3]
y = myfun(x)
//
type(myfun)
typeof(myfun)
//
deff(header,body,"n")
type(myfun)
typeof(myfun)

/////////////////////////////////////////////////////
// The execstr function is more powerful than deff.
// http://code.google.com/p/tumbi/wiki/ScilabNspMainDifferences#deff
// Same example as before, this time with deff.
// The only differences:
//  * header = "function " + header
//  * footer = "endfunction"
//
header = "function y = myfun(x)"
body = [
"y(1) = 2*x(1)+x(2)-x(3)^2"
"y(2) = 2*x(2) + x(3)"
]
footer = "endfunction"
instr = [
header
body
footer
]
execstr(instr)
//
x = [1 2 3]
y = myfun(x)



/////////////////////////////////////////////////////
// A plotting function : TODO...

/////////////////////////////////////////////////////
// Section "A practical optimization example"
//

// This example requires the uncprb module.
if ( ~atomsIsInstalled("uncprb") ) then
    atomsInstall("uncprb")
    atomsLoad("uncprb")
end

// 1. A practical use of execstr

nprob = 1;
[n,m,x0] = uncprb_getinitf(nprob);
//
// Define a wrapper over the objective function for the optimizer
header = "function [fout,gout,ind]=objfun(x,ind)";
body = [
"fout=uncprb_getobjfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
"gout=uncprb_getgrdfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
];
footer = "endfunction"
instr = [
header
body
footer
]
execstr(instr)
//
[fopt,xopt]=optim(objfun,x0)

//
// 2. A practical use of functions with extra-arguments
//
function [f,g,ind]=costfun(x,ind,nprob,n,m)
    [n,m,x0] = uncprb_getinitf(nprob)
    // Make a column vector
    x = x(:)
    f = uncprb_getobjfcn(n,m,x,nprob)
    g = uncprb_getgrdfcn(n,m,x,nprob)
endfunction

nprob = 1;
[n,m,x0] = uncprb_getinitf(nprob);
myobjfun = list(costfun,nprob,n,m);
[fopt,xopt]=optim(myobjfun,x0)

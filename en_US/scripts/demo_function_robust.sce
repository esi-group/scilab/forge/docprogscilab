// Copyright (C) 2010 - Michael Baudin

  // http://bugzilla.scilab.org/show_bug.cgi?id=7670
  // http://en.wikipedia.org/wiki/Pascal_matrix

function P = pascalup_notrobust ( n )
  P = eye(n,n)
  P(1,:) = ones(1,n)
  for i = 2:(n-1)
    P(2:i,i+1) = P(1:(i-1),i)+P(2:i,i)
  end
endfunction

pascalup_notrobust ( 5 )

pascalup_notrobust(-1)
pascalup_notrobust(1.5)

function P = pascalup ( n )
  //
  // Check number of arguments
  //
  [lhs, rhs] = argn()
  if ( rhs <> 1 ) then
    lstr = gettext("%s: Wrong number of input arguments: %d to %d expected, but %d provided.\n")
    error ( msprintf(lstr,"pascalup",1,1,rhs))
  end
  //
  // Check type of arguments
  //
  if ( typeof(n) <> "constant" ) then
    lstr = gettext("%s: Wrong type for input argument #%d: %s expected, but %s provided.\n")
    error ( msprintf(lstr,"pascalup",1,"constant",typeof(n)))
  end
  //
  // Check size of arguments
  //
  if ( size(n,"*") <> 1 ) then
    lstr = gettext("%s: Wrong size for input argument #%d: %d entries expected, but %d provided.\n")
    error ( msprintf(lstr,"pascalup",1,1,size(n,"*")))
  end
  //
  // Check content of arguments
  //
  if ( imag(n)<>0 ) then
    lstr = gettext("%s: Wrong content for input argument #%d: complex numbers are forbidden.\n")
    error ( msprintf(lstr,"pascalup",1))
  end
  if ( n < 0 ) then
    lstr = gettext("%s: Wrong content for input argument #%d: positive entries only are expected.\n")
    error ( msprintf(lstr,"pascalup",1))
  end
  if ( floor(n)<>n ) then
    lstr = gettext("%s: Wrong content of input argument #%d: argument is expected to be a flint.")
    error ( msprintf(lstr,"specfun_pascal",1))
  end
  //
  P = eye(n,n)
  P(1,:) = ones(1,n)
  for i = 2:(n-1)
    P(2:i,i+1) = P(1:(i-1),i)+P(2:i,i)
  end
endfunction



expected = [
    1    1    1    1    1  
    0    1    2    3    4  
    0    0    1    3    6  
    0    0    0    1    4  
    0    0    0    0    1  
];

pascalup ( 5 )

// Generate various errors
pascalup ( ) // Wrong rhs
pascalup ( "a" ) // Wrong type
pascalup ( [1 2] ) // Wrong size
pascalup ( -1 ) // Wrong content
pascalup ( 1.5 ) // Wrong content
pascalup ( 1+%i ) // Wrong content




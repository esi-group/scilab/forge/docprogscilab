// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Benchmarking backslash

// References
// Cleve Moler, "Benchmarks: LINPACK and MATLAB - Fame and fortune from megaflops", 1994, http://www.mathworks.com/company/newsletters/news_notes/pdf/sumfall94cleve.pdf
// "Programming in Scilab", Michael Baudin, 2010, http://forge.scilab.org/index.php/p/docprogscilab/downloads/

/////////////////////////////////////////////////////////////////
//
// A basic benchmark
if ( %f ) then
stacksize("max")
rand( "normal" );
n = 1000;
A = rand(n,n);
b = rand(n,1);
tic();
x = A\b;
t = toc();
mflops = (2/3*n^3 + 2*n^2)/t/1.e6;
disp([n t mflops])
end

/////////////////////////////////////////////////////////////////
//
// A subtle benchmark
scf();

stacksize("max")
rand( "normal" );
s = stacksize();
MB = round(s(1)*8/10^6);
nmax = sqrt(s(1));
mprintf("Memory: %d (MB)\n",MB);
mprintf("Maximum n: %d\n",nmax);

timemin = 0.1;
timemax = 8.0;
nfact = 1.2;

//
// Make a loop over n
xtitle("Backslash","Matrix Order (n)","Megaflops");
n = 1;
k=1;
perftable = [];
while ( %t )
  A = rand(n,n);
  b = rand(n,1);
  c = ceil(log10(1/rcond(A)));
  if ( c > 7 ) then
    // Avoid least square computation.
    // See : http://bugzilla.scilab.org/show_bug.cgi?id=7487
    mprintf("Skipping ill-conditionned matrix: c=%d\n",c);
    continue;
  end
  tic();
  ierr = execstr("x = A\b;","errcatch");
  t = toc();
  if ( ierr <> 0 ) then
    laerr = lasterror();
    disp("Error:");
    disp(laerr);
    break
  end
  if ( t > timemin ) then
    mflops = (2/3*n^3 + 2*n^2)/t/1.e6;
    perftable(k,:) = [n t mflops];
    mprintf("Run #%d: n=%6d, T=%.3f (s), Mflops=%6d, Log(cond)=%2d\n",k,perftable(k,1),perftable(k,2),perftable(k,3),c)
    k = k+1;
    plot(n,mflops,"bo-")
  end
  if ( t > timemax ) then
    break
  end
  n = ceil(nfact * n);
end

// Search for best performance
[M,k] = max(perftable(:,3));
mprintf("Best performance:")
mprintf(" N=%d, T=%.3f, MFLOPS=%d\n",perftable(k,1),perftable(k,2),perftable(k,3));




//
// Plot this perf : [n perf mflops]
if ( %f ) then
thisperf = [
    256.    0.0400576    282.49028  
    292.    0.0333813    502.33424  
    329.    0.0433957    552.06675  
    365.    0.0567483    575.9565  
    402.    0.0734389    594.14098  
    438.    0.0934677    603.43965  
    475.    0.1201728    598.29817  
    512.    0.1535541    586.13058  
    548.    0.1802592    611.96138  
    585.    0.2169787    618.27369  
    621.    0.2603744    616.13836  
    658.    0.300432    635.06152  
    694.    0.3438277    650.90792  
    731.    0.4072523    642.06064  
    768.    0.4740149    639.57803  
    804.    0.5541301    627.59952  
    841.    0.6108784    651.46098  
    877.    0.6943317    649.86565  
    914.    0.7877995    648.2683  
    950.    0.8378715    684.33925  
    987.    0.9547061    673.45492  
    1024.    1.1249509    638.18342  
    1060.    1.1917136    668.1621  
    1097.    1.2985339    679.61257  
    1133.    1.4621024    664.91955  
    1170.    1.9261029    555.77497  
    1206.    1.9194267    610.74249  
    1243.    2.153096    596.0815  
    1280.    2.453528    571.16859  
    1316.    2.6771829    568.83625  
    1353.    3.0644064    540.02972  
    1389.    3.2613563    548.97709  
    1426.    3.5617883    543.89106  
    1462.    3.9456736    529.07831  
    1499.    4.3295589    519.68273  
    1536.    4.5832571    528.14792  
    1572.    5.1640923    502.45872  
    1609.    5.4678624    508.82482  
    1645.    5.7916613    513.32757  
    1682.    6.3124101    503.46052
];
plot(thisperf(:,1),thisperf(:,3),"bo-" )
end

if ( %f ) then
// A pre-stored benchmark
// scilab-5.3.0-beta-4 on Linux Ubuntu 32 bits. 
// CPU : Intel(R) Pentium(R) M processor 2.00GHz at 2 GHz 
// 1 GB of memory. 
// cache size	: 2048 KB
// fpu vme de pse tsc msr pae mce cx8 apic mtrr pge mca cmov clflush dts acpi mmx fxsr sse sse2 ss tm pbe nx up bts est tm2
perfATLAS = [
    590.66823    0.124008    1113.4996  
    708.80187    0.208013    1146.1121  
    850.56225    0.364023    1130.9081  
    1020.6747    0.596037    1192.8125  
    1224.8096    0.996062    1232.7942  
    1469.7716    1.652103    1283.8276  
    1763.7259    2.752172    1331.265  
    2116.4711    4.592287    1378.2623  
    2539.7653    7.788487    1403.9417  
    3047.7183    13.260829    1424.5898  
];

perfRefBlas = [
    492.22352    0.108007    740.59851  
    590.66823    0.208013    663.81841  
    708.80187    0.332021    718.04561  
    850.56225    0.572036    719.66899  
    1020.6747    1.000062    710.91632  
    1224.8096    1.660104    739.67625  
    1469.7716    2.868179    739.49895  
    1763.7259    4.864303    753.21588  
    2116.4711    8.396525    753.80899  
];
xtitle("Backslash","Matrix Order (n)","Megaflops");
plot(perfRefBlas(:,1),perfRefBlas(:,3),"bo-")
plot(perfATLAS(:,1),perfATLAS(:,3),"gx-")
legend ( ["Reference BLAS" "ATLAS" ] );
h = gcf();
h.children.children(1).legend_location = "in_upper_left";

end


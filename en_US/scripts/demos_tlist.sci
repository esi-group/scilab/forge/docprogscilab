// Copyright (C) 2010 - Michael Baudin

// Creating a tlist
p = tlist(["person","firstname","name","birthyear"])
p.firstname = "Paul";
p.name = "Smith";
p.birthyear = 1997;
p

// Get a field
p(2)
fn = p.firstname
fn = getfield(2,p)

// Set a field
p.firstname = "John"
setfield(2,"Ringo",p)
p

// Getting the type 
type(p)
typeof(p)

// Get the fields with fieldnames
fieldnames(p)
p(1)

// Using defined fields
p = tlist(["person","firstname","name","birthyear"])
definedfields(p)
p.firstname = "Paul";
definedfields(p)

// Setting directly the fields at the creation of the object
p = tlist( ..
  ["person","firstname","name","birthyear"], ..
  "Paul", ..
  "Smith", ..
  1997)
p.firstname
p.name
p.birthyear

// Knowing if a field is defined
// 1. Get the matrix of integers representing defined fields
// 2. Search for the index ifield associated with given fieldname
// 3. Search for ifield in the matrix of defined fields
function bool = isfielddef ( tl , fieldname )
  df = definedfields ( tl )
  ifield = find(tl(1)==fieldname)
  k = find(df==ifield)
  bool = ( k <> [] )
endfunction
p = tlist(["person","firstname","name","birthyear"]);
isfielddef ( p , "name" )
p.name = "Smith";
isfielddef ( p , "name" ) 

// Emulating object oriented with typed lists



changelog of the Documentation : Programming in Scilab

docprogscilab  (not released yet)
 * Fixed typos (thanks to Stanislav V. Kroter).

docprogscilab  (v0.9: 2011-09)
 * Fixed typos (thanks to Stanislav V. Kroter).
 * Fixed LaTeX Overfull hboxes.
 * Fixed LaTeX Underfull hboxes.
 * Fixed the "Exercise 5.2 (Kronecker product)", by mentionning 
   the other implementation based on matrix accesses.
   This is now "Exercise 5.2 (Combinations)"
 * Created a section "Repeating rows or columns of a vector"
 * Created a section "Boolean matrix access"
 * Created a section "Linear matrix indexing"
 * Updated the section "Combining vectorized functions" 
   with a link to "Boolean matrix access"
 * Fixed many phrasing issues after second reading.
 * Included comments from Kroter.

docprogscilab  (v0.8: 2011-06)
    * Removed obsolete paragraph in the 
      "Notes and references" of section 3.
    * Added a section on the extraction of hypermatrices.
    * Added the squeeze function.

docprogscilab  (v0.7: 2011-04)
    * Fixed table for the comparison of data types.
    * Cell and struct are mlists.
    * Typo in cell extraction comment.
	* COMPILER is not a variable anymore in Scilab.
	* Fixed a detail in the table of type and typeof.

docprogscilab  (v0.6: 2011-01-13)
    * Added a section for mlist and cell.
    * Added a section for structs and array of structs.
    * Compare data types.

docprogscilab  (v0.5: 2010-12-04)
    * Fixed typos.
    * Updated ATLAS section.
    * Added blocked matrix multiply.
    * Reorganized vectorization principles. 
    * Separated low-level optimization methods.

docprogscilab  (v0.4: 2010-11-17)
    * Fixed typos.

docprogscilab  (v0.3: 2010-11-11)
    * Added Kronecker product exercise.

docprogscilab  (v0.2: 2010-11-03)
    * Fixed typos.
    * Added "Scope of variables" section.

docprogscilab  (v0.1: 2010-10-21)
    * Initial version

